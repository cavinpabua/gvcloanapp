-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2021 at 03:51 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gvc_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `loanID` int(11) NOT NULL,
  `applicantID` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `loanAmount` int(11) NOT NULL,
  `dateApplied` timestamp NULL DEFAULT NULL,
  `isApproved` int(11) NOT NULL DEFAULT 0,
  `approvedBy` int(11) DEFAULT NULL,
  `dateApproved` timestamp NULL DEFAULT NULL,
  `dueDate` timestamp NULL DEFAULT NULL,
  `runningBalance` float NOT NULL,
  `runningNumberOfMonths` int(11) NOT NULL,
  `monthlyPayment` float NOT NULL,
  `numberOfPayments` int(11) NOT NULL,
  `totalInterest` float NOT NULL,
  `totalCostOfLoan` float NOT NULL,
  `isCompleted` int(11) NOT NULL DEFAULT 0,
  `base_rate` float NOT NULL,
  `isRejected` int(11) NOT NULL DEFAULT 0,
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loans`
--

INSERT INTO `loans` (`loanID`, `applicantID`, `createdAt`, `modifiedAt`, `loanAmount`, `dateApplied`, `isApproved`, `approvedBy`, `dateApproved`, `dueDate`, `runningBalance`, `runningNumberOfMonths`, `monthlyPayment`, `numberOfPayments`, `totalInterest`, `totalCostOfLoan`, `isCompleted`, `base_rate`, `isRejected`, `notes`) VALUES
(1, 18, '2021-10-03 03:54:12', '2021-10-03 10:35:05', 10000, '2021-10-03 03:54:12', 1, 1, '2021-10-03 03:54:12', '2022-10-03 03:54:12', 8583.67, 10, 858.333, 12, 300, 10300, 0, 3, 0, ''),
(2, 18, '2021-10-03 06:15:57', '2021-10-03 12:41:10', 1234, '2021-10-03 06:15:57', 1, 1, '0000-00-00 00:00:00', '2022-10-03 06:15:57', 1165.08, 11, 105.918, 12, 37, 1271, 0, 3, 0, ''),
(3, 18, '2021-10-03 06:18:26', '2021-10-03 12:48:04', 33333, '2021-10-03 06:18:26', 0, NULL, NULL, '2022-10-03 06:18:26', 34333, 12, 2861.08, 12, 1000, 34333, 0, 3, 1, 'Pangit kag record'),
(4, 18, '2021-10-03 06:21:25', '2021-10-03 10:19:57', 200, '2021-10-03 06:21:25', 0, NULL, NULL, '2021-11-03 06:21:25', 206, 1, 206, 1, 6, 206, 0, 3, 0, ''),
(5, 18, '2021-10-03 10:22:30', '2021-10-03 10:22:30', 2500, '2021-10-03 10:22:30', 0, NULL, NULL, '2021-12-03 10:22:30', 2575, 2, 1287.5, 2, 75, 2575, 0, 3, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `loan_payments`
--

CREATE TABLE `loan_payments` (
  `paymentID` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `loanID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `adminID` int(11) NOT NULL DEFAULT 1,
  `type` enum('Cash','Cheque') NOT NULL DEFAULT 'Cash',
  `amount` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loan_payments`
--

INSERT INTO `loan_payments` (`paymentID`, `createdAt`, `modifiedAt`, `loanID`, `userID`, `adminID`, `type`, `amount`) VALUES
(1, '2021-10-03 10:27:20', '2021-10-03 10:27:20', 1, 18, 1, 'Cash', 858),
(2, '2021-10-03 10:35:05', '2021-10-03 10:35:05', 1, 18, 1, 'Cheque', 858.333),
(3, '2021-10-03 12:41:10', '2021-10-03 12:41:10', 2, 18, 1, 'Cash', 105.918);

--
-- Triggers `loan_payments`
--
DELIMITER $$
CREATE TRIGGER `payment_trigger` AFTER INSERT ON `loan_payments` FOR EACH ROW UPDATE `loans` set runningBalance=`loans`.runningBalance-NEW.amount, 	runningNumberOfMonths=`loans`.runningNumberOfMonths-1
WHERE loanID=NEW.loanID
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `savings_account`
--

CREATE TABLE `savings_account` (
  `savingsID` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `userID` int(11) NOT NULL,
  `runningBalance` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `savings_account`
--

INSERT INTO `savings_account` (`savingsID`, `createdAt`, `modifiedAt`, `userID`, `runningBalance`) VALUES
(1, '2021-10-02 05:11:55', '2021-10-02 05:12:23', 1, 0),
(6, '2021-10-02 05:45:39', '2021-10-03 06:44:31', 18, 11000);

-- --------------------------------------------------------

--
-- Table structure for table `savings_transaction`
--

CREATE TABLE `savings_transaction` (
  `transactionID` int(11) NOT NULL,
  `savingsID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `type` enum('withdraw','deposit') NOT NULL,
  `amount` float NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `savings_transaction`
--

INSERT INTO `savings_transaction` (`transactionID`, `savingsID`, `userID`, `type`, `amount`, `createdAt`, `modifiedAt`) VALUES
(1, 6, 18, 'withdraw', 10, '2021-10-02 09:23:14', '2021-10-02 09:23:14'),
(2, 6, 18, 'withdraw', 1, '2021-10-02 09:23:27', '2021-10-02 09:23:27'),
(3, 6, 18, 'withdraw', 100, '2021-10-02 09:26:18', '2021-10-02 09:26:18'),
(4, 6, 18, 'withdraw', 2, '2021-10-02 09:30:01', '2021-10-02 09:30:01'),
(5, 6, 18, 'withdraw', 1, '2021-10-02 09:30:04', '2021-10-02 09:30:04'),
(6, 6, 18, 'withdraw', 10, '2021-10-02 09:30:10', '2021-10-02 09:30:10'),
(7, 6, 18, 'withdraw', 20, '2021-10-02 09:31:49', '2021-10-02 09:31:49'),
(8, 6, 18, 'deposit', 1000, '2021-10-02 15:49:32', '2021-10-02 15:49:32'),
(9, 6, 18, 'withdraw', 10000, '2021-10-02 15:49:42', '2021-10-02 15:49:42'),
(10, 6, 18, 'deposit', 46, '2021-10-02 15:49:51', '2021-10-02 15:49:51'),
(11, 6, 18, 'deposit', 98, '2021-10-02 15:49:57', '2021-10-02 15:49:57'),
(12, 6, 18, 'deposit', 5000, '2021-10-03 06:44:27', '2021-10-03 06:44:27'),
(13, 6, 18, 'deposit', 5000, '2021-10-03 06:44:31', '2021-10-03 06:44:31');

--
-- Triggers `savings_transaction`
--
DELIMITER $$
CREATE TRIGGER `deposit` AFTER INSERT ON `savings_transaction` FOR EACH ROW if (NEW.type = "deposit") THEN
    UPDATE `savings_account` set runningBalance=`savings_account`.runningBalance+NEW.amount WHERE userID=NEW.userID;
END IF
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `withdrawal` AFTER INSERT ON `savings_transaction` FOR EACH ROW if (NEW.type = "withdraw") THEN
    UPDATE `savings_account` set runningBalance=`savings_account`.runningBalance-NEW.amount WHERE userID=NEW.userID;
END IF
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `app_name` varchar(500) NOT NULL,
  `base_rate` float NOT NULL,
  `maintenance_mode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `app_name`, `base_rate`, `maintenance_mode`) VALUES
(1, 'GVC LOAN SYSTEM', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userID` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `username` varchar(100) NOT NULL,
  `password` varchar(500) NOT NULL,
  `modifiedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `dob` varchar(50) NOT NULL,
  `maritalStatus` varchar(250) NOT NULL,
  `sourceOfIncome` text NOT NULL,
  `userLevel` varchar(50) NOT NULL DEFAULT 'Client'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `createdAt`, `username`, `password`, `modifiedAt`, `firstName`, `lastName`, `dob`, `maritalStatus`, `sourceOfIncome`, `userLevel`) VALUES
(1, '2021-10-02 05:11:55', 'administrator', 'ZQBYYRijB2I/ZzItK7Ms5nL8qPEo128sayzafrkQvjg=', '2021-10-02 06:37:01', 'Admin', 'Go', '1998-03-03', 'Single', 'Owner', 'Administrator'),
(18, '2021-10-02 05:45:39', 'cavin123', '8wD8bZP7IJlpp2nyIwLm7g==', '2021-10-03 13:09:23', 'Cavin', 'Pabua', '1989-02-01 00:00:00', 'Single', 'Work', 'Client');

--
-- Triggers `users`
--
DELIMITER $$
CREATE TRIGGER `CreateSavingsAccount` AFTER INSERT ON `users` FOR EACH ROW INSERT INTO `savings_account` (`userID`,`runningBalance`) VALUES(NEW.userID, 0)
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`loanID`),
  ADD KEY `approvedBy` (`approvedBy`),
  ADD KEY `applicantID` (`applicantID`);

--
-- Indexes for table `loan_payments`
--
ALTER TABLE `loan_payments`
  ADD PRIMARY KEY (`paymentID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `adminID` (`adminID`),
  ADD KEY `loanID` (`loanID`);

--
-- Indexes for table `savings_account`
--
ALTER TABLE `savings_account`
  ADD PRIMARY KEY (`savingsID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `savings_transaction`
--
ALTER TABLE `savings_transaction`
  ADD PRIMARY KEY (`transactionID`),
  ADD KEY `savingsID` (`savingsID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `loanID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `loan_payments`
--
ALTER TABLE `loan_payments`
  MODIFY `paymentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `savings_account`
--
ALTER TABLE `savings_account`
  MODIFY `savingsID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `savings_transaction`
--
ALTER TABLE `savings_transaction`
  MODIFY `transactionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `loans`
--
ALTER TABLE `loans`
  ADD CONSTRAINT `loans_ibfk_1` FOREIGN KEY (`applicantID`) REFERENCES `users` (`userID`) ON UPDATE CASCADE;

--
-- Constraints for table `loan_payments`
--
ALTER TABLE `loan_payments`
  ADD CONSTRAINT `loan_payments_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `loan_payments_ibfk_2` FOREIGN KEY (`adminID`) REFERENCES `users` (`userID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `loan_payments_ibfk_3` FOREIGN KEY (`loanID`) REFERENCES `loans` (`loanID`) ON UPDATE CASCADE;

--
-- Constraints for table `savings_account`
--
ALTER TABLE `savings_account`
  ADD CONSTRAINT `savings_account_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON UPDATE CASCADE;

--
-- Constraints for table `savings_transaction`
--
ALTER TABLE `savings_transaction`
  ADD CONSTRAINT `savings_transaction_ibfk_1` FOREIGN KEY (`savingsID`) REFERENCES `savings_account` (`savingsID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `savings_transaction_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
