﻿Imports MySql.Data.MySqlClient
Public Class frmLoanAccepter
    Private Sub frnLoanAccepter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        con.Open()
        cmd = New MySqlCommand("SELECT * FROM loans WHERE loanID=@loanID LIMIT 1", con)
        With cmd
            .Parameters.AddWithValue("@loanID", frmSelectedToAcceptLoanID)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                txtBoxTotalInterest.Text = dr.Item("totalInterest").ToString
                txtBoxmonthlyPayment.Text = dr.Item("monthlyPayment").ToString
                txtBoxNumberOfPayment.Text = dr.Item("numberOfPayments").ToString
                txtBoxTotalCostOfLoan.Text = dr.Item("totalCostOfLoan").ToString
                txtBoxUserIDSelect.Text = dr.Item("applicantID").ToString
                loanAmount.Text = dr.Item("loanAmount").ToString
                numberOfMonths.Value = dr.Item("numberOfPayments").ToString
                interestRate.Value = dr.Item("base_rate").ToString
            End While
        End If
        con.Close()
    End Sub

    Private Sub Guna2Button3_Click(sender As Object, e As EventArgs) Handles Guna2Button3.Click
        Me.Close()
    End Sub

    Private Sub btnDeposit_Click(sender As Object, e As EventArgs) Handles btnDeposit.Click
        If MsgBox("Accept Loan Request?", vbQuestion + vbYesNo) = vbYes Then
            con.Open()
            cmd = New MySqlCommand("UPDATE loans set isApproved=1, dateApproved=@dateApproved,approvedBy=1 WHERE loanID=@loanID", con)
            With cmd
                .Parameters.AddWithValue("@loanID", frmSelectedToAcceptLoanID)
                .Parameters.AddWithValue("@dateApproved", DateTime.Now)
                .ExecuteNonQuery()
            End With
            con.Close()
            Me.Close()
            MsgBox("Loan Accepted!")
            frmLoanApplications.loadLoanRequests()
        End If
    End Sub

    Private Sub Guna2Button1_Click(sender As Object, e As EventArgs) Handles Guna2Button1.Click
        If MsgBox("Reject Loan Request?", vbQuestion + vbYesNo) = vbYes Then
            If rejectNote.Text <> "" Then
                con.Open()
                cmd = New MySqlCommand("UPDATE loans set isRejected=1, notes=@notes WHERE loanID=@loanID", con)
                With cmd
                    .Parameters.AddWithValue("@loanID", frmSelectedToAcceptLoanID)
                    .Parameters.AddWithValue("@notes", rejectNote.Text)
                    .ExecuteNonQuery()
                End With
                con.Close()
                Me.Close()
                MsgBox("Loan Rejected!")
                frmLoanApplications.loadLoanRequests()
            Else
                MsgBox("Please input rejection Note", vbExclamation)
            End If

        End If
    End Sub

    Private Sub Guna2Button2_Click(sender As Object, e As EventArgs) Handles Guna2Button2.Click
        frmViewSavingsAccount.Show()
    End Sub
End Class