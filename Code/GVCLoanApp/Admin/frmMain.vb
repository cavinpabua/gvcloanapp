﻿
Public Class userFrmMain
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        picUser.Left = (Panel3.Width - picUser.Width) / 2
        openCon()
        SystemLabel.Text = initSystemName()
        lblUsername.Text = userFirstName + " " + userLastName
        lblUserLevel.Text = userRole

        With frmUsers
            .Width = MainPanel.Width
            .Height = MainPanel.Height
            .TopLevel = False
            MainPanel.Controls.Add(frmUsers)
            .BringToFront()
            .Show()
        End With

    End Sub

    Sub CloseForms()
        For i As Integer = My.Application.OpenForms.Count - 1 To 0 Step -1
            If My.Application.OpenForms(i) IsNot Me Then My.Application.OpenForms(i).Dispose()
        Next
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        For Each f As Form In My.Application.OpenForms
            If f.Name = frmUsers.Name Then Return
        Next
        CloseForms()
        With frmUsers
            .Width = MainPanel.Width
            .Height = MainPanel.Height
            .TopLevel = False
            MainPanel.Controls.Add(frmUsers)
            .BringToFront()
            .Show()
        End With
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        CloseForms()
        Me.Hide()
        userFrmLogin.Show()
    End Sub

    Private Sub MainPanel_Paint(sender As Object, e As PaintEventArgs) Handles MainPanel.Paint

    End Sub

    Private Sub btnLoanApplications_Click(sender As Object, e As EventArgs) Handles btnLoanApplications.Click
        For Each f As Form In My.Application.OpenForms
            If f.Name = frmLoanApplications.Name Then Return
        Next
        CloseForms()
        With frmLoanApplications
            .Width = MainPanel.Width
            .Height = MainPanel.Height
            .TopLevel = False
            MainPanel.Controls.Add(frmLoanApplications)
            .BringToFront()
            .Show()
        End With
    End Sub

    Private Sub btnSavingsAccount_Click(sender As Object, e As EventArgs) Handles btnSavingsAccount.Click
        For Each f As Form In My.Application.OpenForms
            If f.Name = frmSavingsAccount.Name Then Return
        Next
        CloseForms()
        With frmSavingsAccount
            .Width = MainPanel.Width
            .Height = MainPanel.Height
            .TopLevel = False
            MainPanel.Controls.Add(frmSavingsAccount)
            .BringToFront()
            .Show()
        End With
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles SystemLabel.Click

    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        For Each f As Form In My.Application.OpenForms
            If f.Name = frmSettings.Name Then Return
        Next
        CloseForms()
        With frmSettings
            .Width = MainPanel.Width
            .Height = MainPanel.Height
            .TopLevel = False
            MainPanel.Controls.Add(frmSettings)
            .BringToFront()
            .Show()
        End With
    End Sub

    Private Sub btnDashboard_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub lblUsername_Click(sender As Object, e As EventArgs) Handles lblUsername.Click

    End Sub
End Class
