﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmSavingsAccount
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblSavingsAccount = New System.Windows.Forms.Label()
        Me.MetroTabControl1 = New MetroFramework.Controls.MetroTabControl()
        Me.MetroTabPage2 = New MetroFramework.Controls.MetroTabPage()
        Me.UserInfoLineBox = New System.Windows.Forms.TextBox()
        Me.btnShowDepositHistory = New Guna.UI2.WinForms.Guna2Button()
        Me.btnDeposit = New Guna.UI2.WinForms.Guna2Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.depositAmount = New Guna.UI2.WinForms.Guna2TextBox()
        Me.btnRegister = New Guna.UI2.WinForms.Guna2Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBoxUserID = New Guna.UI2.WinForms.Guna2TextBox()
        Me.MetroTabPage3 = New MetroFramework.Controls.MetroTabPage()
        Me.btnShowWithdrawalHistory = New Guna.UI2.WinForms.Guna2Button()
        Me.SavingsInfoBox2 = New System.Windows.Forms.TextBox()
        Me.btnWithdraw = New Guna.UI2.WinForms.Guna2Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SavingsWithdrawalAmount = New Guna.UI2.WinForms.Guna2TextBox()
        Me.selectUserWithdrawal = New Guna.UI2.WinForms.Guna2Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtUserID = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Panel1.SuspendLayout()
        Me.MetroTabControl1.SuspendLayout()
        Me.MetroTabPage2.SuspendLayout()
        Me.MetroTabPage3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblSavingsAccount)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(929, 50)
        Me.Panel1.TabIndex = 0
        '
        'lblSavingsAccount
        '
        Me.lblSavingsAccount.AutoSize = True
        Me.lblSavingsAccount.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSavingsAccount.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblSavingsAccount.Location = New System.Drawing.Point(15, 14)
        Me.lblSavingsAccount.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSavingsAccount.Name = "lblSavingsAccount"
        Me.lblSavingsAccount.Size = New System.Drawing.Size(156, 28)
        Me.lblSavingsAccount.TabIndex = 2
        Me.lblSavingsAccount.Text = "Savings Account"
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage2)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage3)
        Me.MetroTabControl1.ItemSize = New System.Drawing.Size(100, 34)
        Me.MetroTabControl1.Location = New System.Drawing.Point(13, 60)
        Me.MetroTabControl1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MetroTabControl1.Multiline = True
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 0
        Me.MetroTabControl1.Size = New System.Drawing.Size(899, 506)
        Me.MetroTabControl1.TabIndex = 1
        Me.MetroTabControl1.UseSelectable = True
        '
        'MetroTabPage2
        '
        Me.MetroTabPage2.Controls.Add(Me.UserInfoLineBox)
        Me.MetroTabPage2.Controls.Add(Me.btnShowDepositHistory)
        Me.MetroTabPage2.Controls.Add(Me.btnDeposit)
        Me.MetroTabPage2.Controls.Add(Me.Label2)
        Me.MetroTabPage2.Controls.Add(Me.depositAmount)
        Me.MetroTabPage2.Controls.Add(Me.btnRegister)
        Me.MetroTabPage2.Controls.Add(Me.Label1)
        Me.MetroTabPage2.Controls.Add(Me.txtBoxUserID)
        Me.MetroTabPage2.HorizontalScrollbarBarColor = True
        Me.MetroTabPage2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.HorizontalScrollbarSize = 1
        Me.MetroTabPage2.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage2.Name = "MetroTabPage2"
        Me.MetroTabPage2.Size = New System.Drawing.Size(891, 464)
        Me.MetroTabPage2.TabIndex = 1
        Me.MetroTabPage2.Text = "Deposit"
        Me.MetroTabPage2.VerticalScrollbarBarColor = True
        Me.MetroTabPage2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.VerticalScrollbarSize = 2
        '
        'UserInfoLineBox
        '
        Me.UserInfoLineBox.BackColor = System.Drawing.Color.White
        Me.UserInfoLineBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.UserInfoLineBox.Location = New System.Drawing.Point(5, 150)
        Me.UserInfoLineBox.Multiline = True
        Me.UserInfoLineBox.Name = "UserInfoLineBox"
        Me.UserInfoLineBox.Size = New System.Drawing.Size(383, 127)
        Me.UserInfoLineBox.TabIndex = 11
        '
        'btnShowDepositHistory
        '
        Me.btnShowDepositHistory.BackColor = System.Drawing.SystemColors.Control
        Me.btnShowDepositHistory.CheckedState.Parent = Me.btnShowDepositHistory
        Me.btnShowDepositHistory.CustomImages.Parent = Me.btnShowDepositHistory
        Me.btnShowDepositHistory.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.btnShowDepositHistory.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShowDepositHistory.ForeColor = System.Drawing.Color.White
        Me.btnShowDepositHistory.HoverState.Parent = Me.btnShowDepositHistory
        Me.btnShowDepositHistory.Location = New System.Drawing.Point(5, 283)
        Me.btnShowDepositHistory.Name = "btnShowDepositHistory"
        Me.btnShowDepositHistory.ShadowDecoration.Parent = Me.btnShowDepositHistory
        Me.btnShowDepositHistory.Size = New System.Drawing.Size(132, 45)
        Me.btnShowDepositHistory.TabIndex = 10
        Me.btnShowDepositHistory.Text = "Show History"
        Me.btnShowDepositHistory.Visible = False
        '
        'btnDeposit
        '
        Me.btnDeposit.BackColor = System.Drawing.SystemColors.Control
        Me.btnDeposit.CheckedState.Parent = Me.btnDeposit
        Me.btnDeposit.CustomImages.Parent = Me.btnDeposit
        Me.btnDeposit.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.btnDeposit.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeposit.ForeColor = System.Drawing.Color.White
        Me.btnDeposit.HoverState.Parent = Me.btnDeposit
        Me.btnDeposit.Location = New System.Drawing.Point(469, 99)
        Me.btnDeposit.Name = "btnDeposit"
        Me.btnDeposit.ShadowDecoration.Parent = Me.btnDeposit
        Me.btnDeposit.Size = New System.Drawing.Size(132, 45)
        Me.btnDeposit.TabIndex = 8
        Me.btnDeposit.Text = "Deposit"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(464, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 25)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Amount"
        '
        'depositAmount
        '
        Me.depositAmount.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.depositAmount.BorderColor = System.Drawing.Color.DarkGray
        Me.depositAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.depositAmount.DefaultText = "0"
        Me.depositAmount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.depositAmount.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.depositAmount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.depositAmount.DisabledState.Parent = Me.depositAmount
        Me.depositAmount.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.depositAmount.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.depositAmount.FocusedState.Parent = Me.depositAmount
        Me.depositAmount.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.depositAmount.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.depositAmount.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.depositAmount.HoverState.Parent = Me.depositAmount
        Me.depositAmount.Location = New System.Drawing.Point(469, 46)
        Me.depositAmount.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.depositAmount.MinimumSize = New System.Drawing.Size(383, 44)
        Me.depositAmount.Name = "depositAmount"
        Me.depositAmount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.depositAmount.PlaceholderText = "Amount"
        Me.depositAmount.SelectedText = ""
        Me.depositAmount.SelectionStart = 1
        Me.depositAmount.ShadowDecoration.Parent = Me.depositAmount
        Me.depositAmount.Size = New System.Drawing.Size(383, 44)
        Me.depositAmount.TabIndex = 6
        '
        'btnRegister
        '
        Me.btnRegister.BackColor = System.Drawing.SystemColors.Control
        Me.btnRegister.CheckedState.Parent = Me.btnRegister
        Me.btnRegister.CustomImages.Parent = Me.btnRegister
        Me.btnRegister.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.btnRegister.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegister.ForeColor = System.Drawing.Color.White
        Me.btnRegister.HoverState.Parent = Me.btnRegister
        Me.btnRegister.Location = New System.Drawing.Point(5, 99)
        Me.btnRegister.Name = "btnRegister"
        Me.btnRegister.ShadowDecoration.Parent = Me.btnRegister
        Me.btnRegister.Size = New System.Drawing.Size(132, 45)
        Me.btnRegister.TabIndex = 1
        Me.btnRegister.Text = "Select User"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(0, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 25)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "User ID"
        '
        'txtBoxUserID
        '
        Me.txtBoxUserID.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxUserID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxUserID.DefaultText = ""
        Me.txtBoxUserID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxUserID.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxUserID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxUserID.DisabledState.Parent = Me.txtBoxUserID
        Me.txtBoxUserID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxUserID.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxUserID.FocusedState.Parent = Me.txtBoxUserID
        Me.txtBoxUserID.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxUserID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxUserID.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxUserID.HoverState.Parent = Me.txtBoxUserID
        Me.txtBoxUserID.Location = New System.Drawing.Point(5, 46)
        Me.txtBoxUserID.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxUserID.Name = "txtBoxUserID"
        Me.txtBoxUserID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxUserID.PlaceholderText = "User ID"
        Me.txtBoxUserID.SelectedText = ""
        Me.txtBoxUserID.ShadowDecoration.Parent = Me.txtBoxUserID
        Me.txtBoxUserID.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxUserID.TabIndex = 0
        '
        'MetroTabPage3
        '
        Me.MetroTabPage3.Controls.Add(Me.btnShowWithdrawalHistory)
        Me.MetroTabPage3.Controls.Add(Me.SavingsInfoBox2)
        Me.MetroTabPage3.Controls.Add(Me.btnWithdraw)
        Me.MetroTabPage3.Controls.Add(Me.Label3)
        Me.MetroTabPage3.Controls.Add(Me.SavingsWithdrawalAmount)
        Me.MetroTabPage3.Controls.Add(Me.selectUserWithdrawal)
        Me.MetroTabPage3.Controls.Add(Me.Label4)
        Me.MetroTabPage3.Controls.Add(Me.txtUserID)
        Me.MetroTabPage3.HorizontalScrollbarBarColor = True
        Me.MetroTabPage3.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage3.HorizontalScrollbarSize = 1
        Me.MetroTabPage3.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage3.Name = "MetroTabPage3"
        Me.MetroTabPage3.Size = New System.Drawing.Size(891, 527)
        Me.MetroTabPage3.TabIndex = 2
        Me.MetroTabPage3.Text = "Withdrawal"
        Me.MetroTabPage3.VerticalScrollbarBarColor = True
        Me.MetroTabPage3.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage3.VerticalScrollbarSize = 2
        '
        'btnShowWithdrawalHistory
        '
        Me.btnShowWithdrawalHistory.BackColor = System.Drawing.SystemColors.Control
        Me.btnShowWithdrawalHistory.CheckedState.Parent = Me.btnShowWithdrawalHistory
        Me.btnShowWithdrawalHistory.CustomImages.Parent = Me.btnShowWithdrawalHistory
        Me.btnShowWithdrawalHistory.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.btnShowWithdrawalHistory.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShowWithdrawalHistory.ForeColor = System.Drawing.Color.White
        Me.btnShowWithdrawalHistory.HoverState.Parent = Me.btnShowWithdrawalHistory
        Me.btnShowWithdrawalHistory.Location = New System.Drawing.Point(5, 283)
        Me.btnShowWithdrawalHistory.Name = "btnShowWithdrawalHistory"
        Me.btnShowWithdrawalHistory.ShadowDecoration.Parent = Me.btnShowWithdrawalHistory
        Me.btnShowWithdrawalHistory.Size = New System.Drawing.Size(132, 45)
        Me.btnShowWithdrawalHistory.TabIndex = 19
        Me.btnShowWithdrawalHistory.Text = "Show History"
        Me.btnShowWithdrawalHistory.Visible = False
        '
        'SavingsInfoBox2
        '
        Me.SavingsInfoBox2.BackColor = System.Drawing.Color.White
        Me.SavingsInfoBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SavingsInfoBox2.Location = New System.Drawing.Point(5, 152)
        Me.SavingsInfoBox2.Multiline = True
        Me.SavingsInfoBox2.Name = "SavingsInfoBox2"
        Me.SavingsInfoBox2.Size = New System.Drawing.Size(383, 125)
        Me.SavingsInfoBox2.TabIndex = 18
        '
        'btnWithdraw
        '
        Me.btnWithdraw.BackColor = System.Drawing.SystemColors.Control
        Me.btnWithdraw.CheckedState.Parent = Me.btnWithdraw
        Me.btnWithdraw.CustomImages.Parent = Me.btnWithdraw
        Me.btnWithdraw.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.btnWithdraw.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnWithdraw.ForeColor = System.Drawing.Color.White
        Me.btnWithdraw.HoverState.Parent = Me.btnWithdraw
        Me.btnWithdraw.Location = New System.Drawing.Point(469, 101)
        Me.btnWithdraw.Name = "btnWithdraw"
        Me.btnWithdraw.ShadowDecoration.Parent = Me.btnWithdraw
        Me.btnWithdraw.Size = New System.Drawing.Size(132, 45)
        Me.btnWithdraw.TabIndex = 16
        Me.btnWithdraw.Text = "Withdraw"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(464, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 25)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Amount"
        '
        'SavingsWithdrawalAmount
        '
        Me.SavingsWithdrawalAmount.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SavingsWithdrawalAmount.BorderColor = System.Drawing.Color.DarkGray
        Me.SavingsWithdrawalAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SavingsWithdrawalAmount.DefaultText = "0"
        Me.SavingsWithdrawalAmount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.SavingsWithdrawalAmount.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.SavingsWithdrawalAmount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.SavingsWithdrawalAmount.DisabledState.Parent = Me.SavingsWithdrawalAmount
        Me.SavingsWithdrawalAmount.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.SavingsWithdrawalAmount.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.SavingsWithdrawalAmount.FocusedState.Parent = Me.SavingsWithdrawalAmount
        Me.SavingsWithdrawalAmount.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SavingsWithdrawalAmount.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.SavingsWithdrawalAmount.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.SavingsWithdrawalAmount.HoverState.Parent = Me.SavingsWithdrawalAmount
        Me.SavingsWithdrawalAmount.Location = New System.Drawing.Point(469, 48)
        Me.SavingsWithdrawalAmount.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.SavingsWithdrawalAmount.MinimumSize = New System.Drawing.Size(383, 44)
        Me.SavingsWithdrawalAmount.Name = "SavingsWithdrawalAmount"
        Me.SavingsWithdrawalAmount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.SavingsWithdrawalAmount.PlaceholderText = "Amount"
        Me.SavingsWithdrawalAmount.SelectedText = ""
        Me.SavingsWithdrawalAmount.SelectionStart = 1
        Me.SavingsWithdrawalAmount.ShadowDecoration.Parent = Me.SavingsWithdrawalAmount
        Me.SavingsWithdrawalAmount.Size = New System.Drawing.Size(383, 44)
        Me.SavingsWithdrawalAmount.TabIndex = 14
        '
        'selectUserWithdrawal
        '
        Me.selectUserWithdrawal.BackColor = System.Drawing.SystemColors.Control
        Me.selectUserWithdrawal.CheckedState.Parent = Me.selectUserWithdrawal
        Me.selectUserWithdrawal.CustomImages.Parent = Me.selectUserWithdrawal
        Me.selectUserWithdrawal.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.selectUserWithdrawal.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.selectUserWithdrawal.ForeColor = System.Drawing.Color.White
        Me.selectUserWithdrawal.HoverState.Parent = Me.selectUserWithdrawal
        Me.selectUserWithdrawal.Location = New System.Drawing.Point(5, 101)
        Me.selectUserWithdrawal.Name = "selectUserWithdrawal"
        Me.selectUserWithdrawal.ShadowDecoration.Parent = Me.selectUserWithdrawal
        Me.selectUserWithdrawal.Size = New System.Drawing.Size(132, 45)
        Me.selectUserWithdrawal.TabIndex = 11
        Me.selectUserWithdrawal.Text = "Select User"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(0, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 25)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "User ID"
        '
        'txtUserID
        '
        Me.txtUserID.BorderColor = System.Drawing.Color.DarkGray
        Me.txtUserID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtUserID.DefaultText = ""
        Me.txtUserID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtUserID.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtUserID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtUserID.DisabledState.Parent = Me.txtUserID
        Me.txtUserID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtUserID.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtUserID.FocusedState.Parent = Me.txtUserID
        Me.txtUserID.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUserID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtUserID.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtUserID.HoverState.Parent = Me.txtUserID
        Me.txtUserID.Location = New System.Drawing.Point(5, 48)
        Me.txtUserID.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtUserID.PlaceholderText = "User ID"
        Me.txtUserID.SelectedText = ""
        Me.txtUserID.ShadowDecoration.Parent = Me.txtUserID
        Me.txtUserID.Size = New System.Drawing.Size(383, 44)
        Me.txtUserID.TabIndex = 10
        '
        'frmSavingsAccount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(929, 584)
        Me.ControlBox = False
        Me.Controls.Add(Me.MetroTabControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "frmSavingsAccount"
        Me.Text = "frmSavingsAccount"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.MetroTabControl1.ResumeLayout(False)
        Me.MetroTabPage2.ResumeLayout(False)
        Me.MetroTabPage2.PerformLayout()
        Me.MetroTabPage3.ResumeLayout(False)
        Me.MetroTabPage3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblSavingsAccount As Label
    Friend WithEvents MetroTabControl1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents MetroTabPage2 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage3 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents Label1 As Label
    Friend WithEvents txtBoxUserID As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents btnRegister As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents btnDeposit As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Label2 As Label
    Friend WithEvents depositAmount As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents btnWithdraw As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Label3 As Label
    Friend WithEvents SavingsWithdrawalAmount As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents selectUserWithdrawal As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Label4 As Label
    Friend WithEvents txtUserID As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents SavingsInfoBox2 As TextBox
    Friend WithEvents btnShowDepositHistory As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents btnShowWithdrawalHistory As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents UserInfoLineBox As TextBox
End Class
