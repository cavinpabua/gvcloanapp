﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewSavingsAccount
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.savingsBalanceText = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.depositHistory = New Guna.UI2.WinForms.Guna2DataGridView()
        Me.transactionID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnDeposit = New Guna.UI2.WinForms.Guna2Button()
        Me.Panel3.SuspendLayout()
        CType(Me.depositHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(807, 52)
        Me.Panel1.TabIndex = 5
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.savingsBalanceText)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(0, 52)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(807, 75)
        Me.Panel3.TabIndex = 34
        '
        'savingsBalanceText
        '
        Me.savingsBalanceText.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.savingsBalanceText.AutoSize = True
        Me.savingsBalanceText.Location = New System.Drawing.Point(47, 38)
        Me.savingsBalanceText.Name = "savingsBalanceText"
        Me.savingsBalanceText.Size = New System.Drawing.Size(16, 17)
        Me.savingsBalanceText.TabIndex = 36
        Me.savingsBalanceText.Text = "0"
        Me.savingsBalanceText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(42, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(172, 17)
        Me.Label7.TabIndex = 35
        Me.Label7.Text = "Savings Account Balance:"
        '
        'depositHistory
        '
        Me.depositHistory.AllowUserToAddRows = False
        Me.depositHistory.AllowUserToDeleteRows = False
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.White
        Me.depositHistory.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle10
        Me.depositHistory.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.depositHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.depositHistory.BackgroundColor = System.Drawing.Color.White
        Me.depositHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.depositHistory.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.depositHistory.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.depositHistory.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.depositHistory.ColumnHeadersHeight = 27
        Me.depositHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.transactionID, Me.amount, Me.type, Me.TransactionDate})
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.depositHistory.DefaultCellStyle = DataGridViewCellStyle12
        Me.depositHistory.EnableHeadersVisualStyles = False
        Me.depositHistory.GridColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.depositHistory.Location = New System.Drawing.Point(12, 143)
        Me.depositHistory.Name = "depositHistory"
        Me.depositHistory.ReadOnly = True
        Me.depositHistory.RowHeadersVisible = False
        Me.depositHistory.RowHeadersWidth = 51
        Me.depositHistory.RowTemplate.Height = 24
        Me.depositHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.depositHistory.Size = New System.Drawing.Size(771, 310)
        Me.depositHistory.TabIndex = 35
        Me.depositHistory.Theme = Guna.UI2.WinForms.Enums.DataGridViewPresetThemes.[Default]
        Me.depositHistory.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White
        Me.depositHistory.ThemeStyle.AlternatingRowsStyle.Font = Nothing
        Me.depositHistory.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty
        Me.depositHistory.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty
        Me.depositHistory.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty
        Me.depositHistory.ThemeStyle.BackColor = System.Drawing.Color.White
        Me.depositHistory.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.depositHistory.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.depositHistory.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.depositHistory.ThemeStyle.HeaderStyle.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.depositHistory.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White
        Me.depositHistory.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing
        Me.depositHistory.ThemeStyle.HeaderStyle.Height = 27
        Me.depositHistory.ThemeStyle.ReadOnly = True
        Me.depositHistory.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White
        Me.depositHistory.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.depositHistory.ThemeStyle.RowsStyle.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.depositHistory.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.depositHistory.ThemeStyle.RowsStyle.Height = 24
        Me.depositHistory.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.depositHistory.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        '
        'transactionID
        '
        Me.transactionID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.transactionID.HeaderText = "transactionID"
        Me.transactionID.MinimumWidth = 2
        Me.transactionID.Name = "transactionID"
        Me.transactionID.ReadOnly = True
        Me.transactionID.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.transactionID.Width = 151
        '
        'amount
        '
        Me.amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.amount.HeaderText = "Amount"
        Me.amount.MinimumWidth = 10
        Me.amount.Name = "amount"
        Me.amount.ReadOnly = True
        '
        'type
        '
        Me.type.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.type.HeaderText = "type"
        Me.type.MinimumWidth = 6
        Me.type.Name = "type"
        Me.type.ReadOnly = True
        Me.type.Width = 75
        '
        'TransactionDate
        '
        Me.TransactionDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.TransactionDate.HeaderText = "TransactionDate"
        Me.TransactionDate.MinimumWidth = 10
        Me.TransactionDate.Name = "TransactionDate"
        Me.TransactionDate.ReadOnly = True
        '
        'btnDeposit
        '
        Me.btnDeposit.BackColor = System.Drawing.SystemColors.Control
        Me.btnDeposit.CheckedState.Parent = Me.btnDeposit
        Me.btnDeposit.CustomImages.Parent = Me.btnDeposit
        Me.btnDeposit.FillColor = System.Drawing.Color.IndianRed
        Me.btnDeposit.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeposit.ForeColor = System.Drawing.Color.White
        Me.btnDeposit.HoverState.Parent = Me.btnDeposit
        Me.btnDeposit.Location = New System.Drawing.Point(651, 459)
        Me.btnDeposit.Name = "btnDeposit"
        Me.btnDeposit.ShadowDecoration.Parent = Me.btnDeposit
        Me.btnDeposit.Size = New System.Drawing.Size(132, 45)
        Me.btnDeposit.TabIndex = 36
        Me.btnDeposit.Text = "Close"
        '
        'frmViewSavingsAccount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(807, 527)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnDeposit)
        Me.Controls.Add(Me.depositHistory)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmViewSavingsAccount"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmViewSavingsAccount"
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.depositHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents savingsBalanceText As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents depositHistory As Guna.UI2.WinForms.Guna2DataGridView
    Friend WithEvents transactionID As DataGridViewTextBoxColumn
    Friend WithEvents amount As DataGridViewTextBoxColumn
    Friend WithEvents type As DataGridViewTextBoxColumn
    Friend WithEvents TransactionDate As DataGridViewTextBoxColumn
    Friend WithEvents btnDeposit As Guna.UI2.WinForms.Guna2Button
End Class
