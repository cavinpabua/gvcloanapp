﻿Imports MySql.Data.MySqlClient

Public Class frmViewSavingsAccount
    Private Sub btnDeposit_Click(sender As Object, e As EventArgs) Handles btnDeposit.Click
        Me.Close()
    End Sub

    Private Function loadSavingsBalance(userIDToCheck) As Decimal
        con.Open()
        cmd = New MySqlCommand("SELECT runningBalance FROM savings_account WHERE userID=@userID LIMIT 1", con)
        With cmd
            .Parameters.AddWithValue("@userID", userIDToCheck)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                savingsBalanceText.Text = $"PHP{dr.Item("runningBalance").ToString}"
            End While
        End If
        dr.Close()
        con.Close()

    End Function

    Private Sub frmViewSavingsAccount_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim userIDToCheck = frmLoanAccepter.txtBoxUserIDSelect.Text
        loadSavingsBalance(userIDToCheck)
        con.Open()
        cmd = New MySqlCommand("SELECT transactionID, amount, type, createdAt FROM savings_transaction WHERE userID=@userID ORDER BY transactionID DESC LIMIT 100", con)
        With cmd
            .Parameters.AddWithValue("@userID", userIDToCheck)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                depositHistory.Rows.Add(dr.Item("transactionID").ToString, dr.Item("amount").ToString, dr.Item("type").ToString, dr.Item("createdAt").ToString)
                depositHistory.ClearSelection()
            End While
        End If
        con.Close()
    End Sub
End Class