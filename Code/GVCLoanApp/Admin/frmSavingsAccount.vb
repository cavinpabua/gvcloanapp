﻿Imports MySql.Data.MySqlClient

Public Class frmSavingsAccount
    Private Sub frmSavingsAccount_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click
        selectDepositUser()
    End Sub

    Private Sub txtBoxSavingsID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBoxUserID.KeyPress
        If (Not Char.IsControl(e.KeyChar) _
                     AndAlso (Not Char.IsDigit(e.KeyChar) _
                     AndAlso (e.KeyChar <> ChrW(46)))) Then
            e.Handled = True
        End If
    End Sub

    Private Sub depositAmount_KeyPress(sender As Object, e As KeyPressEventArgs) Handles depositAmount.KeyPress
        If (Not Char.IsControl(e.KeyChar) _
                     AndAlso (Not Char.IsDigit(e.KeyChar) _
                     AndAlso (e.KeyChar <> ChrW(46)))) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtUserID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtUserID.KeyPress
        If (Not Char.IsControl(e.KeyChar) _
                     AndAlso (Not Char.IsDigit(e.KeyChar) _
                     AndAlso (e.KeyChar <> ChrW(46)))) Then
            e.Handled = True
        End If
    End Sub

    Private Sub Guna2TextBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles SavingsWithdrawalAmount.KeyPress
        If (Not Char.IsControl(e.KeyChar) _
                     AndAlso (Not Char.IsDigit(e.KeyChar) _
                     AndAlso (e.KeyChar <> ChrW(46)))) Then
            e.Handled = True
        End If
    End Sub

    Private Function selectDepositUser()
        con.Open()
        cmd = New MySqlCommand("SELECT `users`.userID, `users`.firstName, `users`.lastName, `users`.dob, `users`.maritalStatus, `users`.sourceOfIncome, `users`.createdAt, `savings_account`.runningBalance, `savings_account`.savingsID FROM 
                    `users` JOIN `savings_account` ON `users`.userID=`savings_account`.userID WHERE `users`.userID=@userID LIMIT 1", con)
        With cmd
            .Parameters.AddWithValue("@userID", txtBoxUserID.Text.Trim)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                userIDDepositSelected = dr.Item("userID").ToString
                UserInfoLineBox.Text = $"User ID: {userIDDepositSelected}{Environment.NewLine}First Name: {dr.Item("firstName").ToString}{Environment.NewLine}Last Name: {dr.Item("lastName").ToString}{Environment.NewLine}Source Of Income: {dr.Item("sourceOfIncome").ToString}{Environment.NewLine}Available Balance: {dr.Item("runningBalance").ToString}"
                userIDWithdrawalSelectedRunningBalance = dr.Item("runningBalance")
                savingsIDDepositSelected = dr.Item("savingsID").ToString
            End While
            btnShowDepositHistory.Visible = True
            MsgBox("User Selected!")
        Else
            userIDDepositSelected = ""
            SavingsInfoBox2.Text = ""
            btnShowDepositHistory.Visible = False
            MsgBox("User does not exits", vbExclamation)
        End If
        dr.Close()
        con.Close()
    End Function

    Private Function selectWithdrawalUser()
        con.Open()
        cmd = New MySqlCommand("SELECT `users`.userID, `users`.firstName, `users`.lastName, `users`.dob, `users`.maritalStatus, `users`.sourceOfIncome, `users`.createdAt, `savings_account`.runningBalance, `savings_account`.savingsID FROM 
                    `users` JOIN `savings_account` ON `users`.userID=`savings_account`.userID WHERE `users`.userID=@userID LIMIT 1", con)
        With cmd
            .Parameters.AddWithValue("@userID", txtUserID.Text.Trim)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                userIDWithdrawalSelected = dr.Item("userID").ToString
                SavingsInfoBox2.Text = $"User ID: {userIDWithdrawalSelected}{Environment.NewLine}First Name: {dr.Item("firstName").ToString}{Environment.NewLine}Last Name: {dr.Item("lastName").ToString}{Environment.NewLine}Source Of Income: {dr.Item("sourceOfIncome").ToString}{Environment.NewLine}Available Balance: {dr.Item("runningBalance").ToString}
                                        "
                UserInfoLineBox.Text = $"User ID: {userIDWithdrawalSelected}{Environment.NewLine}First Name: {dr.Item("firstName").ToString}{Environment.NewLine}Last Name: {dr.Item("lastName").ToString}{Environment.NewLine}Source Of Income: {dr.Item("sourceOfIncome").ToString}{Environment.NewLine}Available Balance: {dr.Item("runningBalance").ToString}
                                        "

                userIDWithdrawalSelectedRunningBalance = dr.Item("runningBalance")
                savingsIDWithdrawalSelected = dr.Item("savingsID").ToString
            End While
            btnShowWithdrawalHistory.Visible = True
            MsgBox("User Selected!")
        Else
            userIDWithdrawalSelected = ""
            SavingsInfoBox2.Text = ""
            btnShowWithdrawalHistory.Visible = False
            MsgBox("User does not exits", vbExclamation)
        End If
        dr.Close()
        con.Close()
    End Function

    Private Sub Guna2Button2_Click(sender As Object, e As EventArgs) Handles selectUserWithdrawal.Click
        selectWithdrawalUser()
    End Sub

    Private Sub Guna2TextBox2_TextChanged(sender As Object, e As EventArgs) Handles SavingsWithdrawalAmount.TextChanged

    End Sub

    Private Sub txtUserID_TextChanged(sender As Object, e As EventArgs) Handles txtUserID.TextChanged

    End Sub

    Private Sub SavingsInfoBox2_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnWithdraw_Click(sender As Object, e As EventArgs) Handles btnWithdraw.Click
        If MsgBox("Continue withdrawal?", vbQuestion + vbYesNo) = vbYes Then
            If SavingsInfoBox2.Text.ToString <> "" Then
                If Convert.ToDecimal(SavingsWithdrawalAmount.Text) > 0 Then
                    If Convert.ToDecimal(SavingsWithdrawalAmount.Text) <= userIDWithdrawalSelectedRunningBalance Then
                        con.Open()
                        cmd = New MySqlCommand("INSERT INTO savings_transaction (`savingsID`, `userID`, `type`, `amount`) VALUES (@savingsID, @userID, 'withdraw', @amount)", con)
                        With cmd
                            .Parameters.AddWithValue("@savingsID", savingsIDWithdrawalSelected)
                            .Parameters.AddWithValue("@userID", txtUserID.Text.Trim)
                            .Parameters.AddWithValue("@amount", SavingsWithdrawalAmount.Text)
                            .ExecuteNonQuery()
                        End With

                        cmd = New MySqlCommand("SELECT `users`.userID, `users`.firstName, `users`.lastName, `users`.dob, `users`.maritalStatus, `users`.sourceOfIncome, `users`.createdAt, `savings_account`.runningBalance, `savings_account`.savingsID FROM 
                    `users` JOIN `savings_account` ON `users`.userID=`savings_account`.userID WHERE `users`.userID=@userID LIMIT 1", con)
                        With cmd
                            .Parameters.AddWithValue("@userID", txtUserID.Text.Trim)
                        End With
                        dr = cmd.ExecuteReader
                        If dr.HasRows Then
                            While dr.Read
                                userIDWithdrawalSelected = dr.Item("userID").ToString
                                SavingsInfoBox2.Text = $"User ID: {userIDWithdrawalSelected}{Environment.NewLine}First Name: {dr.Item("firstName").ToString}{Environment.NewLine}Last Name: {dr.Item("lastName").ToString}{Environment.NewLine}Source Of Income: {dr.Item("sourceOfIncome").ToString}{Environment.NewLine}Available Balance: {dr.Item("runningBalance").ToString}
                                        "
                                userIDWithdrawalSelectedRunningBalance = dr.Item("runningBalance")
                                savingsIDWithdrawalSelected = dr.Item("savingsID").ToString
                            End While
                            btnShowWithdrawalHistory.Visible = True
                        End If
                        dr.Close()
                        con.Close()
                        MsgBox("Withdrawal Successful")
                    Else
                        MsgBox("Not enough balance for withdrawal.", vbExclamation)
                    End If
                Else
                    MsgBox("Withdrawal must be greater than 0", vbExclamation)
                End If
            Else
                MsgBox("Please select user first.")
            End If
        End If


    End Sub

    Private Sub btnDeposit_Click(sender As Object, e As EventArgs) Handles btnDeposit.Click
        If MsgBox("Continue deposit?", vbQuestion + vbYesNo) = vbYes Then
            If UserInfoLineBox.Text.ToString <> "" Then
                If Convert.ToDecimal(depositAmount.Text) > 0 Then
                    If Convert.ToDecimal(depositAmount.Text) > 0 Then
                        con.Open()
                        cmd = New MySqlCommand("INSERT INTO savings_transaction (`savingsID`, `userID`, `type`, `amount`) VALUES (@savingsID, @userID, 'deposit', @amount)", con)
                        With cmd
                            .Parameters.AddWithValue("@savingsID", savingsIDDepositSelected)
                            .Parameters.AddWithValue("@userID", txtBoxUserID.Text)
                            .Parameters.AddWithValue("@amount", depositAmount.Text)
                            .ExecuteNonQuery()
                        End With

                        cmd = New MySqlCommand("SELECT `users`.userID, `users`.firstName, `users`.lastName, `users`.dob, `users`.maritalStatus, `users`.sourceOfIncome, `users`.createdAt, `savings_account`.runningBalance, `savings_account`.savingsID FROM 
                    `users` JOIN `savings_account` ON `users`.userID=`savings_account`.userID WHERE `users`.userID=@userID LIMIT 1", con)
                        With cmd
                            .Parameters.AddWithValue("@userID", txtBoxUserID.Text)
                        End With
                        dr = cmd.ExecuteReader
                        If dr.HasRows Then
                            While dr.Read
                                userIDDepositSelected = dr.Item("userID").ToString
                                UserInfoLineBox.Text = $"User ID: {userIDDepositSelected}{Environment.NewLine}First Name: {dr.Item("firstName").ToString}{Environment.NewLine}Last Name: {dr.Item("lastName").ToString}{Environment.NewLine}Source Of Income: {dr.Item("sourceOfIncome").ToString}{Environment.NewLine}Available Balance: {dr.Item("runningBalance").ToString}"
                                userIDWithdrawalSelectedRunningBalance = dr.Item("runningBalance")
                                savingsIDDepositSelected = dr.Item("savingsID").ToString
                            End While
                            btnShowDepositHistory.Visible = True
                        End If
                        dr.Close()
                        con.Close()
                        MsgBox("Deposit Successful")
                    Else
                        MsgBox("Not enough balance for Deposit.", vbExclamation)
                    End If
                Else
                    MsgBox("Deposit must be greater than 0", vbExclamation)
                End If
            Else
                MsgBox("Please select user first.")
            End If
        End If
    End Sub

    Private Sub btnShowWithdrawalHistory_Click(sender As Object, e As EventArgs) Handles btnShowWithdrawalHistory.Click
        frmSavingsHistoryWithdrawal.Show()
    End Sub

    Private Sub btnShowDepositHistory_Click(sender As Object, e As EventArgs) Handles btnShowDepositHistory.Click
        frmSavingsHistoryDeposit.Show()
    End Sub
End Class