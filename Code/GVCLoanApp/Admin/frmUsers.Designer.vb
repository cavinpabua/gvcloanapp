﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmUsers
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.UsersTab1 = New MetroFramework.Controls.MetroTabControl()
        Me.MetroTabPage1 = New MetroFramework.Controls.MetroTabPage()
        Me.btnRefreshUserList = New Guna.UI2.WinForms.Guna2Button()
        Me.UserListGrid = New System.Windows.Forms.DataGridView()
        Me.userID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.username = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.firstName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lastName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dob = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.maritalStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RegisterUser = New MetroFramework.Controls.MetroTabPage()
        Me.btnRegister = New Guna.UI2.WinForms.Guna2Button()
        Me.txtBoxPassword = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtBoxSourceOfIncome = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.comboBoxMaritalStatus = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dateTimePickerDOB = New Guna.UI2.WinForms.Guna2DateTimePicker()
        Me.txtBoxFirstname = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBoxLastName = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBoxUsername = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblUsersModule = New System.Windows.Forms.Label()
        Me.Guna2Button2 = New Guna.UI2.WinForms.Guna2Button()
        Me.searchBox = New Guna.UI2.WinForms.Guna2TextBox()
        Me.UsersTab1.SuspendLayout()
        Me.MetroTabPage1.SuspendLayout()
        CType(Me.UserListGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RegisterUser.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'UsersTab1
        '
        Me.UsersTab1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UsersTab1.Controls.Add(Me.MetroTabPage1)
        Me.UsersTab1.Controls.Add(Me.RegisterUser)
        Me.UsersTab1.Location = New System.Drawing.Point(13, 56)
        Me.UsersTab1.Name = "UsersTab1"
        Me.UsersTab1.SelectedIndex = 0
        Me.UsersTab1.Size = New System.Drawing.Size(905, 516)
        Me.UsersTab1.TabIndex = 1
        Me.UsersTab1.UseSelectable = True
        '
        'MetroTabPage1
        '
        Me.MetroTabPage1.Controls.Add(Me.searchBox)
        Me.MetroTabPage1.Controls.Add(Me.Guna2Button2)
        Me.MetroTabPage1.Controls.Add(Me.btnRefreshUserList)
        Me.MetroTabPage1.Controls.Add(Me.UserListGrid)
        Me.MetroTabPage1.HorizontalScrollbarBarColor = True
        Me.MetroTabPage1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.HorizontalScrollbarSize = 2
        Me.MetroTabPage1.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage1.Name = "MetroTabPage1"
        Me.MetroTabPage1.Size = New System.Drawing.Size(897, 474)
        Me.MetroTabPage1.TabIndex = 0
        Me.MetroTabPage1.Text = "User Lists"
        Me.MetroTabPage1.VerticalScrollbarBarColor = True
        Me.MetroTabPage1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.VerticalScrollbarSize = 2
        '
        'btnRefreshUserList
        '
        Me.btnRefreshUserList.CheckedState.Parent = Me.btnRefreshUserList
        Me.btnRefreshUserList.CustomImages.Parent = Me.btnRefreshUserList
        Me.btnRefreshUserList.FillColor = System.Drawing.Color.Transparent
        Me.btnRefreshUserList.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRefreshUserList.ForeColor = System.Drawing.Color.Black
        Me.btnRefreshUserList.HoverState.Parent = Me.btnRefreshUserList
        Me.btnRefreshUserList.Location = New System.Drawing.Point(796, 60)
        Me.btnRefreshUserList.Name = "btnRefreshUserList"
        Me.btnRefreshUserList.ShadowDecoration.Parent = Me.btnRefreshUserList
        Me.btnRefreshUserList.Size = New System.Drawing.Size(98, 23)
        Me.btnRefreshUserList.TabIndex = 3
        Me.btnRefreshUserList.Text = "Refresh List"
        '
        'UserListGrid
        '
        Me.UserListGrid.AllowUserToAddRows = False
        Me.UserListGrid.AllowUserToDeleteRows = False
        Me.UserListGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UserListGrid.BackgroundColor = System.Drawing.Color.White
        Me.UserListGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.UserListGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.UserListGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.userID, Me.username, Me.firstName, Me.lastName, Me.dob, Me.maritalStatus})
        Me.UserListGrid.Location = New System.Drawing.Point(3, 92)
        Me.UserListGrid.Name = "UserListGrid"
        Me.UserListGrid.ReadOnly = True
        Me.UserListGrid.RowHeadersWidth = 51
        Me.UserListGrid.RowTemplate.Height = 24
        Me.UserListGrid.Size = New System.Drawing.Size(891, 355)
        Me.UserListGrid.TabIndex = 2
        '
        'userID
        '
        Me.userID.HeaderText = "User ID"
        Me.userID.MinimumWidth = 6
        Me.userID.Name = "userID"
        Me.userID.ReadOnly = True
        Me.userID.Width = 125
        '
        'username
        '
        Me.username.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.username.HeaderText = "Username"
        Me.username.MinimumWidth = 6
        Me.username.Name = "username"
        Me.username.ReadOnly = True
        '
        'firstName
        '
        Me.firstName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.firstName.HeaderText = "First Name"
        Me.firstName.MinimumWidth = 6
        Me.firstName.Name = "firstName"
        Me.firstName.ReadOnly = True
        '
        'lastName
        '
        Me.lastName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lastName.HeaderText = "Last Name"
        Me.lastName.MinimumWidth = 6
        Me.lastName.Name = "lastName"
        Me.lastName.ReadOnly = True
        '
        'dob
        '
        Me.dob.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dob.HeaderText = "Date of Birth"
        Me.dob.MinimumWidth = 6
        Me.dob.Name = "dob"
        Me.dob.ReadOnly = True
        '
        'maritalStatus
        '
        Me.maritalStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.maritalStatus.HeaderText = "Marital Status"
        Me.maritalStatus.MinimumWidth = 6
        Me.maritalStatus.Name = "maritalStatus"
        Me.maritalStatus.ReadOnly = True
        '
        'RegisterUser
        '
        Me.RegisterUser.Controls.Add(Me.btnRegister)
        Me.RegisterUser.Controls.Add(Me.txtBoxPassword)
        Me.RegisterUser.Controls.Add(Me.Label7)
        Me.RegisterUser.Controls.Add(Me.txtBoxSourceOfIncome)
        Me.RegisterUser.Controls.Add(Me.Label6)
        Me.RegisterUser.Controls.Add(Me.comboBoxMaritalStatus)
        Me.RegisterUser.Controls.Add(Me.Label5)
        Me.RegisterUser.Controls.Add(Me.Label4)
        Me.RegisterUser.Controls.Add(Me.dateTimePickerDOB)
        Me.RegisterUser.Controls.Add(Me.txtBoxFirstname)
        Me.RegisterUser.Controls.Add(Me.Label3)
        Me.RegisterUser.Controls.Add(Me.txtBoxLastName)
        Me.RegisterUser.Controls.Add(Me.Label2)
        Me.RegisterUser.Controls.Add(Me.Label1)
        Me.RegisterUser.Controls.Add(Me.txtBoxUsername)
        Me.RegisterUser.HorizontalScrollbarBarColor = True
        Me.RegisterUser.HorizontalScrollbarHighlightOnWheel = False
        Me.RegisterUser.HorizontalScrollbarSize = 2
        Me.RegisterUser.Location = New System.Drawing.Point(4, 38)
        Me.RegisterUser.Name = "RegisterUser"
        Me.RegisterUser.Size = New System.Drawing.Size(897, 474)
        Me.RegisterUser.TabIndex = 1
        Me.RegisterUser.Text = "Register User"
        Me.RegisterUser.VerticalScrollbarBarColor = True
        Me.RegisterUser.VerticalScrollbarHighlightOnWheel = False
        Me.RegisterUser.VerticalScrollbarSize = 3
        '
        'btnRegister
        '
        Me.btnRegister.BackColor = System.Drawing.SystemColors.Control
        Me.btnRegister.CheckedState.Parent = Me.btnRegister
        Me.btnRegister.CustomImages.Parent = Me.btnRegister
        Me.btnRegister.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.btnRegister.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegister.ForeColor = System.Drawing.Color.White
        Me.btnRegister.HoverState.Parent = Me.btnRegister
        Me.btnRegister.Location = New System.Drawing.Point(23, 424)
        Me.btnRegister.Name = "btnRegister"
        Me.btnRegister.ShadowDecoration.Parent = Me.btnRegister
        Me.btnRegister.Size = New System.Drawing.Size(383, 45)
        Me.btnRegister.TabIndex = 7
        Me.btnRegister.Text = "Save"
        '
        'txtBoxPassword
        '
        Me.txtBoxPassword.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBoxPassword.AutoSize = True
        Me.txtBoxPassword.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxPassword.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxPassword.DefaultText = ""
        Me.txtBoxPassword.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxPassword.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxPassword.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxPassword.DisabledState.Parent = Me.txtBoxPassword
        Me.txtBoxPassword.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxPassword.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxPassword.FocusedState.Parent = Me.txtBoxPassword
        Me.txtBoxPassword.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxPassword.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxPassword.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxPassword.HoverState.Parent = Me.txtBoxPassword
        Me.txtBoxPassword.Location = New System.Drawing.Point(471, 258)
        Me.txtBoxPassword.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxPassword.Name = "txtBoxPassword"
        Me.txtBoxPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxPassword.PlaceholderText = "Password"
        Me.txtBoxPassword.SelectedText = ""
        Me.txtBoxPassword.ShadowDecoration.Parent = Me.txtBoxPassword
        Me.txtBoxPassword.Size = New System.Drawing.Size(347, 44)
        Me.txtBoxPassword.TabIndex = 5
        Me.txtBoxPassword.UseSystemPasswordChar = True
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.White
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(466, 227)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 25)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Password"
        '
        'txtBoxSourceOfIncome
        '
        Me.txtBoxSourceOfIncome.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBoxSourceOfIncome.AutoSize = True
        Me.txtBoxSourceOfIncome.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxSourceOfIncome.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxSourceOfIncome.DefaultText = ""
        Me.txtBoxSourceOfIncome.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxSourceOfIncome.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxSourceOfIncome.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxSourceOfIncome.DisabledState.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxSourceOfIncome.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxSourceOfIncome.FocusedState.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxSourceOfIncome.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxSourceOfIncome.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxSourceOfIncome.HoverState.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.Location = New System.Drawing.Point(471, 167)
        Me.txtBoxSourceOfIncome.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxSourceOfIncome.Name = "txtBoxSourceOfIncome"
        Me.txtBoxSourceOfIncome.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxSourceOfIncome.PlaceholderText = "Source Of Income"
        Me.txtBoxSourceOfIncome.SelectedText = ""
        Me.txtBoxSourceOfIncome.ShadowDecoration.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.Size = New System.Drawing.Size(347, 44)
        Me.txtBoxSourceOfIncome.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(466, 136)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(162, 25)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Source Of Income"
        '
        'comboBoxMaritalStatus
        '
        Me.comboBoxMaritalStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboBoxMaritalStatus.Animated = True
        Me.comboBoxMaritalStatus.BackColor = System.Drawing.Color.Transparent
        Me.comboBoxMaritalStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.comboBoxMaritalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboBoxMaritalStatus.FocusedColor = System.Drawing.Color.Empty
        Me.comboBoxMaritalStatus.FocusedState.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.comboBoxMaritalStatus.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.comboBoxMaritalStatus.FormattingEnabled = True
        Me.comboBoxMaritalStatus.HoverState.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.ItemHeight = 30
        Me.comboBoxMaritalStatus.Items.AddRange(New Object() {"Single", "Married", "Separated", "Divorced", "Widowed"})
        Me.comboBoxMaritalStatus.ItemsAppearance.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.Location = New System.Drawing.Point(471, 70)
        Me.comboBoxMaritalStatus.MinimumSize = New System.Drawing.Size(150, 0)
        Me.comboBoxMaritalStatus.Name = "comboBoxMaritalStatus"
        Me.comboBoxMaritalStatus.ShadowDecoration.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.Size = New System.Drawing.Size(347, 36)
        Me.comboBoxMaritalStatus.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(466, 42)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(127, 25)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Marital Status"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(20, 333)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(118, 25)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Date of Birth"
        '
        'dateTimePickerDOB
        '
        Me.dateTimePickerDOB.CheckedState.Parent = Me.dateTimePickerDOB
        Me.dateTimePickerDOB.FillColor = System.Drawing.Color.Transparent
        Me.dateTimePickerDOB.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateTimePickerDOB.HoverState.Parent = Me.dateTimePickerDOB
        Me.dateTimePickerDOB.Location = New System.Drawing.Point(25, 361)
        Me.dateTimePickerDOB.MaxDate = New Date(9998, 12, 31, 0, 0, 0, 0)
        Me.dateTimePickerDOB.MinDate = New Date(1753, 1, 1, 0, 0, 0, 0)
        Me.dateTimePickerDOB.Name = "dateTimePickerDOB"
        Me.dateTimePickerDOB.ShadowDecoration.Parent = Me.dateTimePickerDOB
        Me.dateTimePickerDOB.Size = New System.Drawing.Size(381, 36)
        Me.dateTimePickerDOB.TabIndex = 6
        Me.dateTimePickerDOB.Value = New Date(2021, 9, 29, 23, 49, 4, 893)
        '
        'txtBoxFirstname
        '
        Me.txtBoxFirstname.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxFirstname.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxFirstname.DefaultText = ""
        Me.txtBoxFirstname.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxFirstname.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxFirstname.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxFirstname.DisabledState.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxFirstname.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxFirstname.FocusedState.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxFirstname.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxFirstname.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxFirstname.HoverState.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.Location = New System.Drawing.Point(23, 164)
        Me.txtBoxFirstname.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxFirstname.Name = "txtBoxFirstname"
        Me.txtBoxFirstname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxFirstname.PlaceholderText = "First Name"
        Me.txtBoxFirstname.SelectedText = ""
        Me.txtBoxFirstname.ShadowDecoration.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxFirstname.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(20, 224)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 25)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Last Name"
        '
        'txtBoxLastName
        '
        Me.txtBoxLastName.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxLastName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxLastName.DefaultText = ""
        Me.txtBoxLastName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxLastName.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxLastName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxLastName.DisabledState.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxLastName.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxLastName.FocusedState.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxLastName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxLastName.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxLastName.HoverState.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.Location = New System.Drawing.Point(25, 255)
        Me.txtBoxLastName.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxLastName.Name = "txtBoxLastName"
        Me.txtBoxLastName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxLastName.PlaceholderText = "Last Name"
        Me.txtBoxLastName.SelectedText = ""
        Me.txtBoxLastName.ShadowDecoration.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.Size = New System.Drawing.Size(381, 44)
        Me.txtBoxLastName.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(18, 133)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(102, 25)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "First Name"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(18, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 25)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Username"
        '
        'txtBoxUsername
        '
        Me.txtBoxUsername.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxUsername.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxUsername.DefaultText = ""
        Me.txtBoxUsername.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxUsername.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxUsername.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxUsername.DisabledState.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxUsername.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxUsername.FocusedState.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxUsername.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxUsername.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxUsername.HoverState.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.Location = New System.Drawing.Point(23, 70)
        Me.txtBoxUsername.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxUsername.Name = "txtBoxUsername"
        Me.txtBoxUsername.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxUsername.PlaceholderText = "Username"
        Me.txtBoxUsername.SelectedText = ""
        Me.txtBoxUsername.ShadowDecoration.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxUsername.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblUsersModule)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(929, 50)
        Me.Panel1.TabIndex = 1
        '
        'lblUsersModule
        '
        Me.lblUsersModule.AutoSize = True
        Me.lblUsersModule.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsersModule.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblUsersModule.Location = New System.Drawing.Point(12, 9)
        Me.lblUsersModule.Name = "lblUsersModule"
        Me.lblUsersModule.Size = New System.Drawing.Size(59, 28)
        Me.lblUsersModule.TabIndex = 0
        Me.lblUsersModule.Text = "Users"
        '
        'Guna2Button2
        '
        Me.Guna2Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Guna2Button2.CheckedState.Parent = Me.Guna2Button2
        Me.Guna2Button2.CustomImages.Parent = Me.Guna2Button2
        Me.Guna2Button2.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Guna2Button2.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Guna2Button2.ForeColor = System.Drawing.Color.White
        Me.Guna2Button2.HoverState.Parent = Me.Guna2Button2
        Me.Guna2Button2.Location = New System.Drawing.Point(283, 40)
        Me.Guna2Button2.Name = "Guna2Button2"
        Me.Guna2Button2.ShadowDecoration.Parent = Me.Guna2Button2
        Me.Guna2Button2.Size = New System.Drawing.Size(72, 32)
        Me.Guna2Button2.TabIndex = 24
        Me.Guna2Button2.Text = "Search"
        '
        'searchBox
        '
        Me.searchBox.BorderColor = System.Drawing.Color.DarkGray
        Me.searchBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.searchBox.DefaultText = ""
        Me.searchBox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.searchBox.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.searchBox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.searchBox.DisabledState.Parent = Me.searchBox
        Me.searchBox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.searchBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.searchBox.FocusedState.Parent = Me.searchBox
        Me.searchBox.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.searchBox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.searchBox.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.searchBox.HoverState.Parent = Me.searchBox
        Me.searchBox.Location = New System.Drawing.Point(0, 33)
        Me.searchBox.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.searchBox.Name = "searchBox"
        Me.searchBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.searchBox.PlaceholderText = "Search"
        Me.searchBox.SelectedText = ""
        Me.searchBox.ShadowDecoration.Parent = Me.searchBox
        Me.searchBox.Size = New System.Drawing.Size(275, 44)
        Me.searchBox.TabIndex = 25
        '
        'frmUsers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(929, 584)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.UsersTab1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "frmUsers"
        Me.UsersTab1.ResumeLayout(False)
        Me.MetroTabPage1.ResumeLayout(False)
        CType(Me.UserListGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RegisterUser.ResumeLayout(False)
        Me.RegisterUser.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents UsersTab1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents MetroTabPage1 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblUsersModule As Label
    Friend WithEvents RegisterUser As MetroFramework.Controls.MetroTabPage
    Friend WithEvents txtBoxUsername As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtBoxLastName As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents txtBoxFirstname As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents dateTimePickerDOB As Guna.UI2.WinForms.Guna2DateTimePicker
    Friend WithEvents comboBoxMaritalStatus As Guna.UI2.WinForms.Guna2ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtBoxPassword As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtBoxSourceOfIncome As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents btnRegister As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents UserListGrid As DataGridView
    Friend WithEvents userID As DataGridViewTextBoxColumn
    Friend WithEvents username As DataGridViewTextBoxColumn
    Friend WithEvents firstName As DataGridViewTextBoxColumn
    Friend WithEvents lastName As DataGridViewTextBoxColumn
    Friend WithEvents dob As DataGridViewTextBoxColumn
    Friend WithEvents maritalStatus As DataGridViewTextBoxColumn
    Friend WithEvents btnRefreshUserList As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Guna2Button2 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents searchBox As Guna.UI2.WinForms.Guna2TextBox
End Class
