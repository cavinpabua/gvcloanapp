﻿Imports MySql.Data.MySqlClient
Public Class frmLoanApplications
    Private Sub frmLoanApplications_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        interestRate.Value = getInterestRate()
        loadLoanRequests()
    End Sub
    Private Sub txtBoxPaymentAmount_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBoxPaymentAmount.KeyPress
        If (Not Char.IsControl(e.KeyChar) _
                     AndAlso (Not Char.IsDigit(e.KeyChar) _
                     AndAlso (e.KeyChar <> ChrW(46)))) Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtBoxUserID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBoxUserID.KeyPress
        If (Not Char.IsControl(e.KeyChar) _
                     AndAlso (Not Char.IsDigit(e.KeyChar) _
                     AndAlso (e.KeyChar <> ChrW(46)))) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtBoxUserID_TextChanged(sender As Object, e As EventArgs) Handles txtBoxUserID.TextChanged

    End Sub

    Private Sub Guna2Button1_Click(sender As Object, e As EventArgs) Handles Guna2Button1.Click
        Dim currentRate = getInterestRate()
        Dim monthsToPay = Convert.ToInt32(numberOfMonths.Value)
        Dim loanAmountToPay = Convert.ToDecimal(loanAmount.Text.ToString)
        Dim monthlyPayment As Decimal
        Dim totalInterest As Decimal
        Dim totalCostOfLoan As Decimal
        monthlyPayment = (loanAmountToPay / monthsToPay) + ((loanAmountToPay / monthsToPay) * (currentRate / 100))
        totalInterest = ((loanAmountToPay / monthsToPay) * (currentRate / 100)) * monthsToPay
        totalCostOfLoan = loanAmountToPay + totalInterest

        txtBoxTotalInterest.Text = totalInterest
        txtBoxmonthlyPayment.Text = monthlyPayment
        txtBoxNumberOfPayment.Text = monthsToPay
        txtBoxTotalCostOfLoan.Text = totalCostOfLoan
    End Sub

    Private Sub Guna2Button2_Click(sender As Object, e As EventArgs) Handles Guna2Button2.Click
        If is_Empty(txtBoxUserIDSelect) Then Return
        If is_Empty(loanAmount) Then Return
        If is_Empty(txtBoxmonthlyPayment) Then Return
        If is_Empty(txtBoxNumberOfPayment) Then Return
        If is_Empty(txtBoxTotalInterest) Then Return
        If is_Empty(txtBoxTotalCostOfLoan) Then Return
        If Convert.ToInt32(txtBoxTotalCostOfLoan.Text.Trim) > 0 Then
            con.Open()
            cmd = New MySqlCommand("SELECT userID FROM users  WHERE userID=@userID LIMIT 1", con)
            With cmd
                .Parameters.AddWithValue("@userID", txtBoxUserIDSelect.Text.Trim)
            End With
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Close()
                con.Close()
                If MsgBox("Continue opening loan?", vbQuestion + vbYesNo) = vbYes Then
                    Dim currentRate = getInterestRate()
                    Dim monthsToPay = Convert.ToInt32(numberOfMonths.Value)
                    Dim loanAmountToPay = Convert.ToDecimal(loanAmount.Text.ToString)
                    Dim monthlyPayment As Decimal
                    Dim totalInterest As Decimal
                    Dim totalCostOfLoan As Decimal
                    monthlyPayment = (loanAmountToPay / monthsToPay) + ((loanAmountToPay / monthsToPay) * (currentRate / 100))
                    totalInterest = ((loanAmountToPay / monthsToPay) * (currentRate / 100)) * monthsToPay
                    totalCostOfLoan = loanAmountToPay + totalInterest
                    con.Open()
                    cmd = New MySqlCommand("INSERT INTO loans (`applicantID`, `loanAmount`,`dateApplied`,`isApproved`, `approvedBy`,`dateApproved`, `dueDate`, `runningBalance`, `monthlyPayment`, `numberOfPayments`, `totalInterest`,`totalCostOfLoan`, `base_rate`, `runningNumberOfMonths`)
                                Values(@applicantID, @loanAmount, @dateApplied, 1, @approvedBy, @dateApproved, @dueDate, @runningBalance, @monthlyPayment, @numberOfPayments, @totalInterest, @totalCostOfLoan, @base_rate, @runningNumberOfMonths) ", con)
                    With cmd
                        .Parameters.AddWithValue("@applicantID", txtBoxUserIDSelect.Text.ToString)
                        .Parameters.AddWithValue("@loanAmount", loanAmountToPay)
                        .Parameters.AddWithValue("@dateApplied", DateTime.Now)
                        .Parameters.AddWithValue("@approvedBy", 1)
                        .Parameters.AddWithValue("@dateApproved", DateTime.Now)
                        .Parameters.AddWithValue("@dueDate", DateTime.Now.AddMonths(monthsToPay))
                        .Parameters.AddWithValue("@runningBalance", totalCostOfLoan)
                        .Parameters.AddWithValue("@monthlyPayment", monthlyPayment)
                        .Parameters.AddWithValue("@numberOfPayments", monthsToPay)
                        .Parameters.AddWithValue("@totalInterest", totalInterest)
                        .Parameters.AddWithValue("@totalCostOfLoan", totalCostOfLoan)
                        .Parameters.AddWithValue("@base_rate", currentRate)
                        .Parameters.AddWithValue("@runningNumberOfMonths", monthsToPay)
                        .ExecuteNonQuery()
                    End With
                    con.Close()
                    loanAmount.Text = ""
                    txtBoxmonthlyPayment.Text = ""
                    txtBoxNumberOfPayment.Text = ""
                    txtBoxTotalInterest.Text = ""
                    txtBoxTotalCostOfLoan.Text = ""
                    MsgBox("Successfully Requested Loan")
                End If
            Else
                MsgBox("No user found", vbExclamation)
                con.Close()
            End If
        Else
            MsgBox("Total Cost Loan must be above zero", vbExclamation)
        End If

    End Sub

    Private Function SelectUserToPay()
        Guna2DataGridView1.Rows.Clear()
        con.Open()
        cmd = New MySqlCommand("SELECT loanID, dateApproved, loanAmount, isCompleted FROM loans WHERE applicantID=@userID AND isApproved=1 ORDER BY isCompleted ASC", con)
        With cmd
            .Parameters.AddWithValue("@userID", txtBoxUserID.Text.Trim)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                Dim status As String
                If dr.Item("isCompleted").ToString = "0" Then
                    status = "Active"
                Else
                    status = "Completed"
                End If
                Guna2DataGridView1.Rows.Add(
                    dr.Item("loanID").ToString,
                    Convert.ToDateTime(dr.Item("dateApproved").ToString).ToString("MMM dd, yyyy"),
                    $"PHP{dr.Item("loanAmount").ToString}",
                    status,
                    "View",
                    "Select"
                    )
            End While
        End If
        con.Close()
    End Function

    Private Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click
        SelectUserToPay()

    End Sub

    Private Sub Guna2DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles Guna2DataGridView1.CellContentClick
        Dim columnName As String = Guna2DataGridView1.Columns(e.ColumnIndex).Name
        If columnName = "view" Then
            Dim selectedLoanID = Guna2DataGridView1.Rows(e.RowIndex).Cells(0).Value.ToString
            frmSelectedToViewLoanID = selectedLoanID
            frmLoanViewInfo.Show()
        End If

        If columnName = "selectLoan" Then
            Dim selectedLoanID = Guna2DataGridView1.Rows(e.RowIndex).Cells(0).Value.ToString
            frmSelectedToPayLoanID = selectedLoanID
            con.Open()
            cmd = New MySqlCommand("SELECT * FROM `loans` WHERE loanID=@loanID LIMIT 1", con)
            With cmd
                .Parameters.AddWithValue("@loanID", frmSelectedToPayLoanID)
            End With
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    Guna2TextBox1.Text = dr.Item("loanAmount").ToString
                    Guna2TextBox2.Text = dr.Item("monthlyPayment").ToString
                    Guna2TextBox3.Text = dr.Item("totalCostOfLoan").ToString
                    txtBoxRemaining.Text = dr.Item("runningBalance").ToString
                    txtBoxPaymentAmount.Text = dr.Item("monthlyPayment").ToString
                End While
            End If
            con.Close()
        End If
    End Sub

    Private Sub btnDeposit_Click(sender As Object, e As EventArgs) Handles btnDeposit.Click

        If Convert.ToDecimal(txtBoxRemaining.Text.Trim) > 0 Then
            If MsgBox("Continue Payment?", vbQuestion + vbYesNo) = vbYes Then
                If Convert.ToDecimal(txtBoxPaymentAmount.Text) > 0 Then
                    con.Open()
                    cmd = New MySqlCommand("INSERT INTO loan_payments (`loanID`, `userID`, `adminID`, `type`, `amount`) VALUES (@loanID, @userID, @adminID,@type,@amount)", con)
                    With cmd
                        .Parameters.AddWithValue("@loanID", frmSelectedToPayLoanID)
                        .Parameters.AddWithValue("@userID", txtBoxUserID.Text.Trim)
                        .Parameters.AddWithValue("@adminID", 1)
                        .Parameters.AddWithValue("@type", paymentType.SelectedItem)
                        .Parameters.AddWithValue("@amount", txtBoxPaymentAmount.Text)
                        .ExecuteNonQuery()
                    End With
                    Dim remaining = Convert.ToDecimal(txtBoxRemaining.Text.Trim)
                    Dim toPayAmount = Convert.ToDecimal(txtBoxPaymentAmount.Text.Trim)
                    remaining = remaining - toPayAmount
                    txtBoxRemaining.Text = remaining.ToString

                    If Convert.ToDecimal(remaining) < Convert.ToDecimal(txtBoxPaymentAmount.Text.Trim) Then
                        cmd = New MySqlCommand("UPDATE loans set isCompleted=1 WHERE loanID=@loanID", con)
                        With cmd
                            .Parameters.AddWithValue("@loanID", frmSelectedToPayLoanID)
                            .ExecuteNonQuery()
                        End With
                    End If
                    con.Close()
                    MsgBox("Payment Successful!")
                End If
            End If

        Else
            MsgBox("Loan already completed!")
        End If
        SelectUserToPay()
    End Sub

    Private Sub Guna2DataGridView2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles Guna2DataGridView2.CellContentClick
        Dim columnName As String = Guna2DataGridView2.Columns(e.ColumnIndex).Name
        If columnName = "viewInfo" Then
            Dim selectedLoanID = Guna2DataGridView2.Rows(e.RowIndex).Cells(0).Value.ToString
            frmSelectedToAcceptLoanID = selectedLoanID
            frmLoanAccepter.Show()
        End If
    End Sub

    Public Function loadLoanRequests()
        Guna2DataGridView2.Rows.Clear()
        con.Open()
        cmd = New MySqlCommand("SELECT * FROM loans JOIN users ON `loans`.applicantID=`users`.userID WHERE isApproved=0 AND isCompleted=0 AND isRejected=0 AND (firstName LIKE @firstName OR lastName LIKE @lastName)", con)
        With cmd
            .Parameters.AddWithValue("@firstName", "%" & searchBox.Text.Trim & "%")
            .Parameters.AddWithValue("@lastName", "%" & searchBox.Text.Trim & "%")
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                Guna2DataGridView2.Rows.Add(
                    dr.Item("loanID"),
                    dr.Item("userID"),
                    $"{dr.Item("firstName")} {dr.Item("lastName")}",
                    dr.Item("loanAmount"),
                    Convert.ToDateTime(dr.Item("dateApplied").ToString).ToString("MMM dd, yyyy"),
                    dr.Item("numberOfPayments"),
                    "View Info"
                    )
            End While
        End If
        con.Close()
    End Function

    Private Sub Guna2Button3_Click(sender As Object, e As EventArgs) Handles Guna2Button3.Click
        loadLoanRequests()
    End Sub

    Private Sub txtBoxUserIDSelect_TextChanged(sender As Object, e As EventArgs) Handles txtBoxUserIDSelect.TextChanged

    End Sub
End Class