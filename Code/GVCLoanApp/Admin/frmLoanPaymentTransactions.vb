﻿Imports MySql.Data.MySqlClient
Public Class frmLoanPaymentTransactions
    Private Sub btnDeposit_Click(sender As Object, e As EventArgs) Handles btnDeposit.Click
        Me.Close()
    End Sub

    Private Sub frmLoanPaymentTransactions_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        con.Open()
        cmd = New MySqlCommand("SELECT * FROM `loan_payments` WHERE loanID=@loanID ORDER BY paymentID DESC LIMIT 100", con)
        With cmd
            .Parameters.AddWithValue("@loanID", frmSelectedToViewLoanID)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                Guna2DataGridView1.Rows.Add(
                    dr.Item("paymentID"),
                    Convert.ToDateTime(dr.Item("createdAt").ToString).ToString("MMM dd, yyyy hh:mm tt"),
                    dr.Item("type"),
                    dr.Item("amount")
                    )
            End While
        End If
        con.Close()
    End Sub

    Private Sub Guna2DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles Guna2DataGridView1.CellContentClick

    End Sub
End Class