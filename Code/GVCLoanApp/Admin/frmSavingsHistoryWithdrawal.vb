﻿Imports MySql.Data.MySqlClient

Public Class frmSavingsHistoryWithdrawal
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub frmSavingsHistory_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        con.Open()
        cmd = New MySqlCommand("SELECT transactionID, amount, type, createdAt FROM savings_transaction WHERE userID=@userID ORDER BY transactionID DESC LIMIT 100", con)
        With cmd
            .Parameters.AddWithValue("@userID", userIDWithdrawalSelected)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                withdrawalHistory.Rows.Add(dr.Item("transactionID").ToString, dr.Item("amount").ToString, dr.Item("type").ToString, dr.Item("createdAt").ToString)
                withdrawalHistory.ClearSelection()
            End While
        End If
        con.Close()

    End Sub

    Private Sub btnDeposit_Click(sender As Object, e As EventArgs) Handles btnDeposit.Click
        Me.Close()
    End Sub
End Class