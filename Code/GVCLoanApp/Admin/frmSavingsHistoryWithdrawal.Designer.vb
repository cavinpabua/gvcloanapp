﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSavingsHistoryWithdrawal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.withdrawalHistory = New Guna.UI2.WinForms.Guna2DataGridView()
        Me.btnDeposit = New Guna.UI2.WinForms.Guna2Button()
        Me.transactionID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.createdDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.withdrawalHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(815, 52)
        Me.Panel1.TabIndex = 0
        '
        'withdrawalHistory
        '
        Me.withdrawalHistory.AllowUserToAddRows = False
        Me.withdrawalHistory.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        Me.withdrawalHistory.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.withdrawalHistory.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.withdrawalHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.withdrawalHistory.BackgroundColor = System.Drawing.Color.White
        Me.withdrawalHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.withdrawalHistory.CausesValidation = False
        Me.withdrawalHistory.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.withdrawalHistory.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 10.5!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.withdrawalHistory.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.withdrawalHistory.ColumnHeadersHeight = 27
        Me.withdrawalHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.transactionID, Me.amount, Me.type, Me.createdDate})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI", 10.5!)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.withdrawalHistory.DefaultCellStyle = DataGridViewCellStyle3
        Me.withdrawalHistory.EnableHeadersVisualStyles = False
        Me.withdrawalHistory.GridColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.withdrawalHistory.Location = New System.Drawing.Point(22, 72)
        Me.withdrawalHistory.Name = "withdrawalHistory"
        Me.withdrawalHistory.ReadOnly = True
        Me.withdrawalHistory.RowHeadersVisible = False
        Me.withdrawalHistory.RowHeadersWidth = 51
        Me.withdrawalHistory.RowTemplate.Height = 24
        Me.withdrawalHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.withdrawalHistory.Size = New System.Drawing.Size(769, 313)
        Me.withdrawalHistory.TabIndex = 1
        Me.withdrawalHistory.Theme = Guna.UI2.WinForms.Enums.DataGridViewPresetThemes.[Default]
        Me.withdrawalHistory.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White
        Me.withdrawalHistory.ThemeStyle.AlternatingRowsStyle.Font = Nothing
        Me.withdrawalHistory.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty
        Me.withdrawalHistory.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty
        Me.withdrawalHistory.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty
        Me.withdrawalHistory.ThemeStyle.BackColor = System.Drawing.Color.White
        Me.withdrawalHistory.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.withdrawalHistory.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.withdrawalHistory.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.withdrawalHistory.ThemeStyle.HeaderStyle.Font = New System.Drawing.Font("Segoe UI", 10.5!)
        Me.withdrawalHistory.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White
        Me.withdrawalHistory.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing
        Me.withdrawalHistory.ThemeStyle.HeaderStyle.Height = 27
        Me.withdrawalHistory.ThemeStyle.ReadOnly = True
        Me.withdrawalHistory.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White
        Me.withdrawalHistory.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.withdrawalHistory.ThemeStyle.RowsStyle.Font = New System.Drawing.Font("Segoe UI", 10.5!)
        Me.withdrawalHistory.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.withdrawalHistory.ThemeStyle.RowsStyle.Height = 24
        Me.withdrawalHistory.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.withdrawalHistory.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        '
        'btnDeposit
        '
        Me.btnDeposit.BackColor = System.Drawing.SystemColors.Control
        Me.btnDeposit.CheckedState.Parent = Me.btnDeposit
        Me.btnDeposit.CustomImages.Parent = Me.btnDeposit
        Me.btnDeposit.FillColor = System.Drawing.Color.IndianRed
        Me.btnDeposit.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeposit.ForeColor = System.Drawing.Color.White
        Me.btnDeposit.HoverState.Parent = Me.btnDeposit
        Me.btnDeposit.Location = New System.Drawing.Point(659, 391)
        Me.btnDeposit.Name = "btnDeposit"
        Me.btnDeposit.ShadowDecoration.Parent = Me.btnDeposit
        Me.btnDeposit.Size = New System.Drawing.Size(132, 45)
        Me.btnDeposit.TabIndex = 9
        Me.btnDeposit.Text = "Close"
        '
        'transactionID
        '
        Me.transactionID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.transactionID.HeaderText = "Transaction ID"
        Me.transactionID.Name = "transactionID"
        Me.transactionID.ReadOnly = True
        Me.transactionID.Width = 150
        '
        'amount
        '
        Me.amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.amount.HeaderText = "Amount"
        Me.amount.MinimumWidth = 10
        Me.amount.Name = "amount"
        Me.amount.ReadOnly = True
        '
        'type
        '
        Me.type.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.type.HeaderText = "Type"
        Me.type.MinimumWidth = 6
        Me.type.Name = "type"
        Me.type.ReadOnly = True
        Me.type.Width = 76
        '
        'createdDate
        '
        Me.createdDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.createdDate.HeaderText = "Transaction Date"
        Me.createdDate.MinimumWidth = 10
        Me.createdDate.Name = "createdDate"
        Me.createdDate.ReadOnly = True
        '
        'frmSavingsHistoryWithdrawal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(815, 466)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnDeposit)
        Me.Controls.Add(Me.withdrawalHistory)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSavingsHistoryWithdrawal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmSavingsHistory"
        CType(Me.withdrawalHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents withdrawalHistory As Guna.UI2.WinForms.Guna2DataGridView
    Friend WithEvents btnDeposit As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents transactionID As DataGridViewTextBoxColumn
    Friend WithEvents amount As DataGridViewTextBoxColumn
    Friend WithEvents type As DataGridViewTextBoxColumn
    Friend WithEvents createdDate As DataGridViewTextBoxColumn
End Class
