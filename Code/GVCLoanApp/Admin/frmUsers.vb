﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography

Public Class frmUsers
    Private Function Encrypt(clearText As String) As String
        Dim EncryptionKey As String = "GVCSPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D,
         &H65, &H64, &H76, &H65, &H64, &H65,
         &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function

    Private Function Decrypt(cipherText As String) As String
        Dim EncryptionKey As String = "GVCSPBNI99212"
        Dim cipherBytes As Byte() = Convert.FromBase64String(cipherText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D,
         &H65, &H64, &H76, &H65, &H64, &H65,
         &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                    cs.Write(cipherBytes, 0, cipherBytes.Length)
                    cs.Close()
                End Using
                cipherText = Encoding.Unicode.GetString(ms.ToArray())
            End Using
        End Using
        Return cipherText
    End Function

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles UserListGrid.CellContentClick

    End Sub

    Private Sub frmUsers_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        UserListGrid.Rows.Clear()
        GetUsersList()
    End Sub
    Private Function GetUsersList() As DataTable

        con.Open()
        cmd = New MySqlCommand("SELECT *  FROM users WHERE (firstName LIKE @searchFirst OR lastName LIKE @searchLast) AND userLevel='Client'", con)
        With cmd
            .Parameters.AddWithValue("@searchFirst", "%" & searchBox.Text.ToString & "%")
            .Parameters.AddWithValue("@searchLast", "%" & searchBox.Text.ToString & "%")
        End With
        dr = cmd.ExecuteReader
        While dr.Read
            UserListGrid.Rows.Add(dr.Item("userID").ToString, dr.Item("username").ToString, dr.Item("firstName").ToString,
                                  dr.Item("lastName").ToString, dr.Item("dob").ToString, dr.Item("maritalStatus").ToString)
            UserListGrid.ClearSelection()
        End While
        dr.Close()
        con.Close()
    End Function

    Private Function CheckUsernameAvailability(username) As Boolean
        Dim resulted As Boolean
        con.Open()
        cmd = New MySqlCommand("SELECT userID FROM users where username=@username", con)
        cmd.Parameters.AddWithValue("@username", username)
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            resulted = False
            MsgBox("Username is already taken.", vbExclamation)
        Else
            resulted = True
        End If
        con.Close()
        Return resulted

    End Function

    Private Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click
        Try
            If is_Empty(txtBoxUsername) = True Then Return
            If is_Empty(txtBoxFirstname) = True Then Return
            If is_Empty(txtBoxLastName) = True Then Return
            If is_Empty(txtBoxSourceOfIncome) = True Then Return
            If is_Empty(txtBoxPassword) = True Then Return
            If CheckUsernameAvailability(txtBoxUsername.Text) = False Then Return
            If txtBoxPassword.Text.Length >= 6 Then
                If MsgBox("Do you want to register this user?", vbQuestion + vbYesNo) = vbYes Then
                    con.Open()
                    cmd = New MySqlCommand("INSERT INTO users (`username`, `firstName`, `lastName`, `dob`, `maritalStatus`,`sourceOfIncome`,`password`) VALUES (@username, @firstName, @lastName, @dob, @maritalStatus, @sourceOfIncome, @password)", con)
                    With cmd
                        .Parameters.AddWithValue("@username", txtBoxUsername.Text)
                        .Parameters.AddWithValue("@firstName", txtBoxFirstname.Text)
                        .Parameters.AddWithValue("@lastName", txtBoxLastName.Text)
                        .Parameters.AddWithValue("@dob", CStr(dateTimePickerDOB.Value.Date))
                        .Parameters.AddWithValue("@maritalStatus", comboBoxMaritalStatus.SelectedItem)
                        .Parameters.AddWithValue("@sourceOfIncome", txtBoxSourceOfIncome.Text)
                        .Parameters.AddWithValue("@password", Encrypt(txtBoxPassword.Text.Trim))
                        .ExecuteNonQuery()


                    End With
                    con.Close()
                    txtBoxUsername.Text = ""
                    txtBoxFirstname.Text = ""
                    txtBoxLastName.Text = ""
                    txtBoxSourceOfIncome.Text = ""
                    txtBoxPassword.Text = ""
                    comboBoxMaritalStatus.SelectedItem = ""
                    MsgBox("Successfully Registered New User!")
                    UserListGrid.Rows.Clear()
                    GetUsersList()
                End If
            Else
                MsgBox("Password must be greater than 6 characters.")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnRefreshUserList_Click(sender As Object, e As EventArgs) Handles btnRefreshUserList.Click
        UserListGrid.Rows.Clear()
        GetUsersList()
    End Sub

    Private Sub Guna2Button2_Click(sender As Object, e As EventArgs) Handles Guna2Button2.Click
        UserListGrid.Rows.Clear()
        GetUsersList()
    End Sub
End Class