﻿Imports MySql.Data.MySqlClient
Public Class frmLoanViewInfo
    Private Sub btnDeposit_Click(sender As Object, e As EventArgs) Handles btnDeposit.Click
        Me.Close()
    End Sub

    Private Function loadSelectedLoanData()
        con.Open()
        cmd = New MySqlCommand("SELECT * FROM `loans` JOIN `users` ON `loans`.applicantID=`users`.userID WHERE loanID=@loanID LIMIT 1", con)
        With cmd
            .Parameters.AddWithValue("@loanID", frmSelectedToViewLoanID)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                Guna2TextBox1.Text = $"{dr.Item("firstName").ToString} {dr.Item("lastName").ToString}"
                loanAmount.Text = dr.Item("loanAmount").ToString
                interestRate.Value = dr.Item("base_rate")
                numberOfMonths.Value = dr.Item("numberOfPayments")
                txtBoxmonthlyPayment.Text = dr.Item("monthlyPayment").ToString
                txtBoxTotalCostOfLoan.Text = dr.Item("totalCostOfLoan").ToString
                txtBoxRemaining.Text = dr.Item("runningBalance").ToString
                Guna2TextBox2.Text = dr.Item("runningNumberOfMonths").ToString
            End While
        End If
        con.Close()

    End Function

    Private Sub frmLoanViewInfo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadSelectedLoanData()
    End Sub

    Private Sub Guna2Button1_Click(sender As Object, e As EventArgs) Handles Guna2Button1.Click
        frmLoanPaymentTransactions.Show()
    End Sub
End Class