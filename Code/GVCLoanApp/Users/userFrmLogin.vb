﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Public Class userFrmLogin
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        openCon()
        frontLabel.Text = initSystemName()
        labelFrontSystem.Text = frontLabel.Text
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        CloseForms()
        Close()
        Application.Exit()
    End Sub

    Sub CloseForms()
        For i As Integer = My.Application.OpenForms.Count - 1 To 0 Step -1
            If My.Application.OpenForms(i) IsNot Me Then My.Application.OpenForms(i).Dispose()
        Next
    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click
        For Each f As Form In My.Application.OpenForms
            If f.Name = userFrmRegister.Name Then Return
        Next
        CloseForms()
        With userFrmRegister
            .Width = loginMainPanel.Width
            .Height = loginMainPanel.Height
            .TopLevel = False
            loginMainPanel.Controls.Add(userFrmRegister)
            .BringToFront()
            .Show()
        End With
    End Sub

    Private Function Encrypt(clearText As String) As String
        Dim EncryptionKey As String = "GVCSPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D,
         &H65, &H64, &H76, &H65, &H64, &H65,
         &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Dim role As String
        con.Open()
        cmd = New MySqlCommand("SELECT * FROM users WHERE username=@username AND password=@password", con)
        With cmd
            .Parameters.AddWithValue("@username", txtBoxUsername.Text.Trim)
            .Parameters.AddWithValue("@password", Encrypt(txtBoxPassword.Text.Trim))
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                role = dr.Item("userLevel").ToString
                userRole = dr.Item("userLevel").ToString
                userID = dr.Item("userID").ToString
                userFirstName = dr.Item("firstName").ToString
                userLastName = dr.Item("lastName").ToString
                userDOB = dr.Item("dob")
                userMaritalStatus = dr.Item("maritalStatus").ToString
                userSourceOfIncome = dr.Item("sourceOfIncome").ToString

            End While
            dr.Close()
            con.Close()
            txtBoxUsername.Text = ""
            txtBoxPassword.Text = ""
            If role = "Administrator" Then
                userFrmMain.Show()
                Me.Close()
            Else
                clientFrmMain.Show()
                Me.Close()
            End If
        Else
            MsgBox("Invalid Username or Password.")
            dr.Close()
            con.Close()
        End If

    End Sub
End Class