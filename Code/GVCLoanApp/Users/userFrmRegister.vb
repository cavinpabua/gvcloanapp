﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography

Public Class userFrmRegister
    Private Function Encrypt(clearText As String) As String
        Dim EncryptionKey As String = "GVCSPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D,
         &H65, &H64, &H76, &H65, &H64, &H65,
         &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function

    Private Function Decrypt(cipherText As String) As String
        Dim EncryptionKey As String = "GVCSPBNI99212"
        Dim cipherBytes As Byte() = Convert.FromBase64String(cipherText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D,
         &H65, &H64, &H76, &H65, &H64, &H65,
         &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                    cs.Write(cipherBytes, 0, cipherBytes.Length)
                    cs.Close()
                End Using
                cipherText = Encoding.Unicode.GetString(ms.ToArray())
            End Using
        End Using
        Return cipherText
    End Function
    Sub CloseForms()
        For i As Integer = My.Application.OpenForms.Count - 1 To 0 Step -1
            If My.Application.OpenForms(i) IsNot Me Then My.Application.OpenForms(i).Dispose()
        Next
    End Sub
    Private Sub Label8_Click(sender As Object, e As EventArgs) Handles Label8.Click
        Me.Dispose()
    End Sub

    Private Function CheckUsernameAvailability(username) As Boolean
        Dim resulted As Boolean
        con.Open()
        cmd = New MySqlCommand("SELECT userID FROM users where username=@username", con)
        cmd.Parameters.AddWithValue("@username", username)
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            resulted = False
            MsgBox("Username is already taken.", vbExclamation)
        Else
            resulted = True
        End If
        con.Close()
        Return resulted

    End Function

    Private Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click
        Try
            If is_Empty(txtBoxUsername) = True Then Return
            If is_Empty(txtBoxFirstname) = True Then Return
            If is_Empty(txtBoxLastName) = True Then Return
            If is_Empty(txtBoxSourceOfIncome) = True Then Return
            If is_Empty(txtBoxPassword) = True Then Return
            If CheckUsernameAvailability(txtBoxUsername.Text) = False Then Return
            If txtBoxPassword.Text.Length >= 6 Then
                If MsgBox("Do you want to register this user?", vbQuestion + vbYesNo) = vbYes Then
                    con.Open()
                    cmd = New MySqlCommand("INSERT INTO users (`username`, `firstName`, `lastName`, `dob`, `maritalStatus`,`sourceOfIncome`,`password`) VALUES (@username, @firstName, @lastName, @dob, @maritalStatus, @sourceOfIncome, @password)", con)
                    With cmd
                        .Parameters.AddWithValue("@username", txtBoxUsername.Text)
                        .Parameters.AddWithValue("@firstName", txtBoxFirstname.Text)
                        .Parameters.AddWithValue("@lastName", txtBoxLastName.Text)
                        .Parameters.AddWithValue("@dob", CStr(dateTimePickerDOB.Value.Date))
                        .Parameters.AddWithValue("@maritalStatus", comboBoxMaritalStatus.SelectedItem)
                        .Parameters.AddWithValue("@sourceOfIncome", txtBoxSourceOfIncome.Text)
                        .Parameters.AddWithValue("@password", Encrypt(txtBoxPassword.Text.Trim))
                        .ExecuteNonQuery()


                    End With
                    con.Close()
                    txtBoxUsername.Text = ""
                    txtBoxFirstname.Text = ""
                    txtBoxLastName.Text = ""
                    txtBoxSourceOfIncome.Text = ""
                    txtBoxPassword.Text = ""
                    comboBoxMaritalStatus.SelectedItem = ""
                    MsgBox("Successfully Registered!")
                End If
            Else
                MsgBox("Password must be greater than 6 characters.")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class