﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class userFrmRegister
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Guna2HtmlLabel1 = New Guna.UI2.WinForms.Guna2HtmlLabel()
        Me.btnRegister = New Guna.UI2.WinForms.Guna2Button()
        Me.txtBoxPassword = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtBoxSourceOfIncome = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.comboBoxMaritalStatus = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dateTimePickerDOB = New Guna.UI2.WinForms.Guna2DateTimePicker()
        Me.txtBoxFirstname = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBoxLastName = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBoxUsername = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Guna2HtmlLabel1
        '
        Me.Guna2HtmlLabel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2HtmlLabel1.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Guna2HtmlLabel1.Location = New System.Drawing.Point(431, 47)
        Me.Guna2HtmlLabel1.Name = "Guna2HtmlLabel1"
        Me.Guna2HtmlLabel1.Size = New System.Drawing.Size(89, 34)
        Me.Guna2HtmlLabel1.TabIndex = 15
        Me.Guna2HtmlLabel1.Text = "Register"
        '
        'btnRegister
        '
        Me.btnRegister.BackColor = System.Drawing.SystemColors.Control
        Me.btnRegister.CheckedState.Parent = Me.btnRegister
        Me.btnRegister.CustomImages.Parent = Me.btnRegister
        Me.btnRegister.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.btnRegister.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegister.ForeColor = System.Drawing.Color.White
        Me.btnRegister.HoverState.Parent = Me.btnRegister
        Me.btnRegister.Location = New System.Drawing.Point(69, 493)
        Me.btnRegister.Name = "btnRegister"
        Me.btnRegister.ShadowDecoration.Parent = Me.btnRegister
        Me.btnRegister.Size = New System.Drawing.Size(383, 45)
        Me.btnRegister.TabIndex = 27
        Me.btnRegister.Text = "Register"
        '
        'txtBoxPassword
        '
        Me.txtBoxPassword.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBoxPassword.AutoSize = True
        Me.txtBoxPassword.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxPassword.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxPassword.DefaultText = ""
        Me.txtBoxPassword.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxPassword.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxPassword.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxPassword.DisabledState.Parent = Me.txtBoxPassword
        Me.txtBoxPassword.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxPassword.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxPassword.FocusedState.Parent = Me.txtBoxPassword
        Me.txtBoxPassword.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxPassword.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxPassword.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxPassword.HoverState.Parent = Me.txtBoxPassword
        Me.txtBoxPassword.Location = New System.Drawing.Point(517, 327)
        Me.txtBoxPassword.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxPassword.Name = "txtBoxPassword"
        Me.txtBoxPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxPassword.PlaceholderText = "Password"
        Me.txtBoxPassword.SelectedText = ""
        Me.txtBoxPassword.ShadowDecoration.Parent = Me.txtBoxPassword
        Me.txtBoxPassword.Size = New System.Drawing.Size(347, 44)
        Me.txtBoxPassword.TabIndex = 24
        Me.txtBoxPassword.UseSystemPasswordChar = True
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.White
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(512, 296)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 25)
        Me.Label7.TabIndex = 32
        Me.Label7.Text = "Password"
        '
        'txtBoxSourceOfIncome
        '
        Me.txtBoxSourceOfIncome.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBoxSourceOfIncome.AutoSize = True
        Me.txtBoxSourceOfIncome.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxSourceOfIncome.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxSourceOfIncome.DefaultText = ""
        Me.txtBoxSourceOfIncome.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxSourceOfIncome.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxSourceOfIncome.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxSourceOfIncome.DisabledState.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxSourceOfIncome.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxSourceOfIncome.FocusedState.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxSourceOfIncome.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxSourceOfIncome.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxSourceOfIncome.HoverState.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.Location = New System.Drawing.Point(517, 236)
        Me.txtBoxSourceOfIncome.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxSourceOfIncome.Name = "txtBoxSourceOfIncome"
        Me.txtBoxSourceOfIncome.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxSourceOfIncome.PlaceholderText = "Source Of Income"
        Me.txtBoxSourceOfIncome.SelectedText = ""
        Me.txtBoxSourceOfIncome.ShadowDecoration.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.Size = New System.Drawing.Size(347, 44)
        Me.txtBoxSourceOfIncome.TabIndex = 23
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(512, 205)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(162, 25)
        Me.Label6.TabIndex = 31
        Me.Label6.Text = "Source Of Income"
        '
        'comboBoxMaritalStatus
        '
        Me.comboBoxMaritalStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboBoxMaritalStatus.Animated = True
        Me.comboBoxMaritalStatus.BackColor = System.Drawing.Color.Transparent
        Me.comboBoxMaritalStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.comboBoxMaritalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboBoxMaritalStatus.FocusedColor = System.Drawing.Color.Empty
        Me.comboBoxMaritalStatus.FocusedState.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.comboBoxMaritalStatus.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.comboBoxMaritalStatus.FormattingEnabled = True
        Me.comboBoxMaritalStatus.HoverState.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.ItemHeight = 30
        Me.comboBoxMaritalStatus.Items.AddRange(New Object() {"Single", "Married", "Separated", "Divorced", "Widowed"})
        Me.comboBoxMaritalStatus.ItemsAppearance.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.Location = New System.Drawing.Point(517, 139)
        Me.comboBoxMaritalStatus.MinimumSize = New System.Drawing.Size(150, 0)
        Me.comboBoxMaritalStatus.Name = "comboBoxMaritalStatus"
        Me.comboBoxMaritalStatus.ShadowDecoration.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.Size = New System.Drawing.Size(347, 36)
        Me.comboBoxMaritalStatus.TabIndex = 21
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(512, 111)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(127, 25)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "Marital Status"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(66, 402)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(118, 25)
        Me.Label4.TabIndex = 29
        Me.Label4.Text = "Date of Birth"
        '
        'dateTimePickerDOB
        '
        Me.dateTimePickerDOB.CheckedState.Parent = Me.dateTimePickerDOB
        Me.dateTimePickerDOB.FillColor = System.Drawing.Color.Transparent
        Me.dateTimePickerDOB.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateTimePickerDOB.HoverState.Parent = Me.dateTimePickerDOB
        Me.dateTimePickerDOB.Location = New System.Drawing.Point(71, 430)
        Me.dateTimePickerDOB.MaxDate = New Date(9998, 12, 31, 0, 0, 0, 0)
        Me.dateTimePickerDOB.MinDate = New Date(1753, 1, 1, 0, 0, 0, 0)
        Me.dateTimePickerDOB.Name = "dateTimePickerDOB"
        Me.dateTimePickerDOB.ShadowDecoration.Parent = Me.dateTimePickerDOB
        Me.dateTimePickerDOB.Size = New System.Drawing.Size(381, 36)
        Me.dateTimePickerDOB.TabIndex = 26
        Me.dateTimePickerDOB.Value = New Date(2021, 9, 29, 23, 49, 4, 893)
        '
        'txtBoxFirstname
        '
        Me.txtBoxFirstname.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxFirstname.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxFirstname.DefaultText = ""
        Me.txtBoxFirstname.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxFirstname.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxFirstname.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxFirstname.DisabledState.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxFirstname.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxFirstname.FocusedState.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxFirstname.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxFirstname.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxFirstname.HoverState.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.Location = New System.Drawing.Point(69, 233)
        Me.txtBoxFirstname.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxFirstname.Name = "txtBoxFirstname"
        Me.txtBoxFirstname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxFirstname.PlaceholderText = "First Name"
        Me.txtBoxFirstname.SelectedText = ""
        Me.txtBoxFirstname.ShadowDecoration.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxFirstname.TabIndex = 19
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(66, 293)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 25)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "Last Name"
        '
        'txtBoxLastName
        '
        Me.txtBoxLastName.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxLastName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxLastName.DefaultText = ""
        Me.txtBoxLastName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxLastName.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxLastName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxLastName.DisabledState.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxLastName.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxLastName.FocusedState.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxLastName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxLastName.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxLastName.HoverState.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.Location = New System.Drawing.Point(71, 324)
        Me.txtBoxLastName.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxLastName.Name = "txtBoxLastName"
        Me.txtBoxLastName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxLastName.PlaceholderText = "Last Name"
        Me.txtBoxLastName.SelectedText = ""
        Me.txtBoxLastName.ShadowDecoration.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.Size = New System.Drawing.Size(381, 44)
        Me.txtBoxLastName.TabIndex = 20
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(64, 202)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(102, 25)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "First Name"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(64, 108)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 25)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Username"
        '
        'txtBoxUsername
        '
        Me.txtBoxUsername.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxUsername.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxUsername.DefaultText = ""
        Me.txtBoxUsername.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxUsername.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxUsername.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxUsername.DisabledState.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxUsername.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxUsername.FocusedState.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxUsername.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxUsername.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxUsername.HoverState.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.Location = New System.Drawing.Point(69, 139)
        Me.txtBoxUsername.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxUsername.Name = "txtBoxUsername"
        Me.txtBoxUsername.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxUsername.PlaceholderText = "Username"
        Me.txtBoxUsername.SelectedText = ""
        Me.txtBoxUsername.ShadowDecoration.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxUsername.TabIndex = 18
        '
        'Label8
        '
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label8.Location = New System.Drawing.Point(60, 47)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(79, 30)
        Me.Label8.TabIndex = 33
        Me.Label8.Text = "<< Login"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'userFrmRegister
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(929, 647)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.btnRegister)
        Me.Controls.Add(Me.txtBoxPassword)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtBoxSourceOfIncome)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.comboBoxMaritalStatus)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dateTimePickerDOB)
        Me.Controls.Add(Me.txtBoxFirstname)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtBoxLastName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtBoxUsername)
        Me.Controls.Add(Me.Guna2HtmlLabel1)
        Me.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "userFrmRegister"
        Me.Text = "userFrmRegister"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Guna2HtmlLabel1 As Guna.UI2.WinForms.Guna2HtmlLabel
    Friend WithEvents btnRegister As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents txtBoxPassword As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtBoxSourceOfIncome As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents comboBoxMaritalStatus As Guna.UI2.WinForms.Guna2ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents dateTimePickerDOB As Guna.UI2.WinForms.Guna2DateTimePicker
    Friend WithEvents txtBoxFirstname As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtBoxLastName As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtBoxUsername As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label8 As Label
End Class
