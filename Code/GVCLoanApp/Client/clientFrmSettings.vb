﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Public Class clientFrmSettings
    Private Function Encrypt(clearText As String) As String
        Dim EncryptionKey As String = "GVCSPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D,
         &H65, &H64, &H76, &H65, &H64, &H65,
         &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function
    Private Sub Guna2Button2_Click(sender As Object, e As EventArgs) Handles Guna2Button2.Click
        If NewPass.Text.Trim = NewPassConfirm.Text.Trim Then
            If NewPass.Text.Trim.Length >= 6 Then
                con.Open()
                cmd = New MySqlCommand("SELECT userID FROM users WHERE userID=@userID AND password=@password", con)
                With cmd
                    .Parameters.AddWithValue("@userID", userID)
                    .Parameters.AddWithValue("@password", Encrypt(oldPass.Text.Trim))
                End With
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Close()
                    cmd = New MySqlCommand("UPDATE users set password=@password WHERE userID=@userID", con)
                    With cmd
                        .Parameters.AddWithValue("@userID", userID)
                        .Parameters.AddWithValue("@password", Encrypt(NewPass.Text.Trim))
                        .ExecuteNonQuery()
                    End With
                    oldPass.Text = ""
                    NewPass.Text = ""
                    NewPassConfirm.Text = ""
                    MsgBox("Successfully Changed Password.")
                Else
                    MsgBox("Invalid old password", vbExclamation)
                End If
                con.Close()
            Else
                MsgBox("Password must be greater than or equal to 6 characters", vbExclamation)
            End If
        Else
            MsgBox("New password and confirmation password does not match!", vbExclamation)
        End If
    End Sub
End Class