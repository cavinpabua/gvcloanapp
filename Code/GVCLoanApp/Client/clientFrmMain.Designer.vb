﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class clientFrmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(clientFrmMain))
        Me.frontLabel = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.clientSystemLabel = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnExit = New Guna.UI2.WinForms.Guna2Button()
        Me.btnSettings = New Guna.UI2.WinForms.Guna2Button()
        Me.btnLoanApplications = New Guna.UI2.WinForms.Guna2Button()
        Me.btnUsers = New Guna.UI2.WinForms.Guna2Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblUserLevel = New System.Windows.Forms.Label()
        Me.lblUsername = New System.Windows.Forms.Label()
        Me.picUser = New System.Windows.Forms.PictureBox()
        Me.clientMainPanel = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.picUser, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'frontLabel
        '
        Me.frontLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.frontLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.frontLabel.Font = New System.Drawing.Font("Segoe UI", 24.0!, System.Drawing.FontStyle.Bold)
        Me.frontLabel.ForeColor = System.Drawing.Color.Gainsboro
        Me.frontLabel.Location = New System.Drawing.Point(0, 0)
        Me.frontLabel.Name = "frontLabel"
        Me.frontLabel.Size = New System.Drawing.Size(1199, 640)
        Me.frontLabel.TabIndex = 1
        Me.frontLabel.Text = "GVC LOAN SYSTEM"
        Me.frontLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(8, Byte), Integer), CType(CType(145, Byte), Integer), CType(CType(178, Byte), Integer))
        Me.Panel1.Controls.Add(Me.clientSystemLabel)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1199, 56)
        Me.Panel1.TabIndex = 3
        '
        'clientSystemLabel
        '
        Me.clientSystemLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.clientSystemLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.clientSystemLabel.Font = New System.Drawing.Font("Segoe UI", 24.0!, System.Drawing.FontStyle.Bold)
        Me.clientSystemLabel.ForeColor = System.Drawing.Color.Gainsboro
        Me.clientSystemLabel.Location = New System.Drawing.Point(0, 0)
        Me.clientSystemLabel.Name = "clientSystemLabel"
        Me.clientSystemLabel.Size = New System.Drawing.Size(1199, 56)
        Me.clientSystemLabel.TabIndex = 0
        Me.clientSystemLabel.Text = "GVC LOAN SYSTEM"
        Me.clientSystemLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Panel2.Controls.Add(Me.btnExit)
        Me.Panel2.Controls.Add(Me.btnSettings)
        Me.Panel2.Controls.Add(Me.btnLoanApplications)
        Me.Panel2.Controls.Add(Me.btnUsers)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 56)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(270, 584)
        Me.Panel2.TabIndex = 4
        '
        'btnExit
        '
        Me.btnExit.Animated = True
        Me.btnExit.CheckedState.Parent = Me.btnExit
        Me.btnExit.CustomImages.Parent = Me.btnExit
        Me.btnExit.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.btnExit.FillColor = System.Drawing.Color.Transparent
        Me.btnExit.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.ForeColor = System.Drawing.Color.White
        Me.btnExit.HoverState.Parent = Me.btnExit
        Me.btnExit.Image = CType(resources.GetObject("btnExit.Image"), System.Drawing.Image)
        Me.btnExit.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.btnExit.ImageSize = New System.Drawing.Size(24, 24)
        Me.btnExit.Location = New System.Drawing.Point(0, 539)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.ShadowDecoration.Parent = Me.btnExit
        Me.btnExit.Size = New System.Drawing.Size(270, 45)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "Logout"
        Me.btnExit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.btnExit.TextOffset = New System.Drawing.Point(20, 0)
        '
        'btnSettings
        '
        Me.btnSettings.Animated = True
        Me.btnSettings.CheckedState.Parent = Me.btnSettings
        Me.btnSettings.CustomImages.Parent = Me.btnSettings
        Me.btnSettings.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSettings.FillColor = System.Drawing.Color.Transparent
        Me.btnSettings.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSettings.ForeColor = System.Drawing.Color.White
        Me.btnSettings.HoverState.Parent = Me.btnSettings
        Me.btnSettings.Image = CType(resources.GetObject("btnSettings.Image"), System.Drawing.Image)
        Me.btnSettings.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.btnSettings.ImageSize = New System.Drawing.Size(24, 24)
        Me.btnSettings.Location = New System.Drawing.Point(0, 353)
        Me.btnSettings.Name = "btnSettings"
        Me.btnSettings.ShadowDecoration.Parent = Me.btnSettings
        Me.btnSettings.Size = New System.Drawing.Size(270, 45)
        Me.btnSettings.TabIndex = 5
        Me.btnSettings.Text = "Settings"
        Me.btnSettings.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.btnSettings.TextOffset = New System.Drawing.Point(20, 0)
        '
        'btnLoanApplications
        '
        Me.btnLoanApplications.Animated = True
        Me.btnLoanApplications.CheckedState.Parent = Me.btnLoanApplications
        Me.btnLoanApplications.CustomImages.Parent = Me.btnLoanApplications
        Me.btnLoanApplications.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnLoanApplications.FillColor = System.Drawing.Color.Transparent
        Me.btnLoanApplications.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoanApplications.ForeColor = System.Drawing.Color.White
        Me.btnLoanApplications.HoverState.Parent = Me.btnLoanApplications
        Me.btnLoanApplications.Image = CType(resources.GetObject("btnLoanApplications.Image"), System.Drawing.Image)
        Me.btnLoanApplications.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.btnLoanApplications.ImageSize = New System.Drawing.Size(24, 24)
        Me.btnLoanApplications.Location = New System.Drawing.Point(0, 308)
        Me.btnLoanApplications.Name = "btnLoanApplications"
        Me.btnLoanApplications.ShadowDecoration.Parent = Me.btnLoanApplications
        Me.btnLoanApplications.Size = New System.Drawing.Size(270, 45)
        Me.btnLoanApplications.TabIndex = 3
        Me.btnLoanApplications.Text = "Loan Applications"
        Me.btnLoanApplications.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.btnLoanApplications.TextOffset = New System.Drawing.Point(20, 0)
        '
        'btnUsers
        '
        Me.btnUsers.Animated = True
        Me.btnUsers.CheckedState.Parent = Me.btnUsers
        Me.btnUsers.CustomImages.Parent = Me.btnUsers
        Me.btnUsers.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnUsers.FillColor = System.Drawing.Color.Transparent
        Me.btnUsers.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUsers.ForeColor = System.Drawing.Color.White
        Me.btnUsers.HoverState.Parent = Me.btnUsers
        Me.btnUsers.Image = CType(resources.GetObject("btnUsers.Image"), System.Drawing.Image)
        Me.btnUsers.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.btnUsers.ImageSize = New System.Drawing.Size(24, 24)
        Me.btnUsers.Location = New System.Drawing.Point(0, 263)
        Me.btnUsers.Name = "btnUsers"
        Me.btnUsers.ShadowDecoration.Parent = Me.btnUsers
        Me.btnUsers.Size = New System.Drawing.Size(270, 45)
        Me.btnUsers.TabIndex = 2
        Me.btnUsers.Text = "User Info"
        Me.btnUsers.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.btnUsers.TextOffset = New System.Drawing.Point(20, 0)
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lblUserLevel)
        Me.Panel3.Controls.Add(Me.lblUsername)
        Me.Panel3.Controls.Add(Me.picUser)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(270, 263)
        Me.Panel3.TabIndex = 0
        '
        'lblUserLevel
        '
        Me.lblUserLevel.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserLevel.ForeColor = System.Drawing.Color.White
        Me.lblUserLevel.Location = New System.Drawing.Point(-4, 219)
        Me.lblUserLevel.Name = "lblUserLevel"
        Me.lblUserLevel.Size = New System.Drawing.Size(270, 25)
        Me.lblUserLevel.TabIndex = 2
        Me.lblUserLevel.Text = "Role"
        Me.lblUserLevel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblUsername
        '
        Me.lblUsername.Font = New System.Drawing.Font("Segoe UI", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsername.ForeColor = System.Drawing.Color.White
        Me.lblUsername.Location = New System.Drawing.Point(0, 189)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(270, 34)
        Me.lblUsername.TabIndex = 1
        Me.lblUsername.Text = "User"
        Me.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picUser
        '
        Me.picUser.Image = CType(resources.GetObject("picUser.Image"), System.Drawing.Image)
        Me.picUser.Location = New System.Drawing.Point(50, 26)
        Me.picUser.Name = "picUser"
        Me.picUser.Size = New System.Drawing.Size(170, 160)
        Me.picUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picUser.TabIndex = 0
        Me.picUser.TabStop = False
        '
        'clientMainPanel
        '
        Me.clientMainPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.clientMainPanel.Location = New System.Drawing.Point(270, 56)
        Me.clientMainPanel.Name = "clientMainPanel"
        Me.clientMainPanel.Size = New System.Drawing.Size(929, 584)
        Me.clientMainPanel.TabIndex = 5
        '
        'clientFrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1199, 640)
        Me.ControlBox = False
        Me.Controls.Add(Me.clientMainPanel)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.frontLabel)
        Me.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "clientFrmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "clientFrmMain"
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.picUser, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents frontLabel As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents clientSystemLabel As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents btnExit As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents btnSettings As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents btnLoanApplications As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents btnUsers As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Panel3 As Panel
    Friend WithEvents lblUserLevel As Label
    Friend WithEvents lblUsername As Label
    Friend WithEvents picUser As PictureBox
    Friend WithEvents clientMainPanel As Panel
End Class
