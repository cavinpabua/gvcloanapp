﻿Imports MySql.Data.MySqlClient
Public Class clientFrmLoanApplication
    Private Sub Guna2Button1_Click(sender As Object, e As EventArgs) Handles Guna2Button1.Click
        Dim currentRate = getInterestRate()
        Dim monthsToPay = Convert.ToInt32(numberOfMonths.Value)
        Dim loanAmountToPay = Convert.ToDecimal(loanAmount.Text.ToString)
        Dim monthlyPayment As Decimal
        Dim totalInterest As Decimal
        Dim totalCostOfLoan As Decimal
        monthlyPayment = (loanAmountToPay / monthsToPay) + ((loanAmountToPay / monthsToPay) * (currentRate / 100))
        totalInterest = ((loanAmountToPay / monthsToPay) * (currentRate / 100)) * monthsToPay
        totalCostOfLoan = loanAmountToPay + totalInterest

        txtBoxTotalInterest.Text = totalInterest
        txtBoxmonthlyPayment.Text = monthlyPayment
        txtBoxNumberOfPayment.Text = monthsToPay
        txtBoxTotalCostOfLoan.Text = totalCostOfLoan
    End Sub

    Private Sub Guna2Button2_Click(sender As Object, e As EventArgs) Handles Guna2Button2.Click
        If is_Empty(loanAmount) Then Return
        If is_Empty(txtBoxmonthlyPayment) Then Return
        If is_Empty(txtBoxNumberOfPayment) Then Return
        If is_Empty(txtBoxTotalInterest) Then Return
        If is_Empty(txtBoxTotalCostOfLoan) Then Return
        con.Open()
        cmd = New MySqlCommand("SELECT userID FROM users  WHERE userID=@userID LIMIT 1", con)
        With cmd
            .Parameters.AddWithValue("@userID", userID)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            dr.Close()
            con.Close()
            If MsgBox("Continue opening loan?", vbQuestion + vbYesNo) = vbYes Then
                Dim currentRate = getInterestRate()
                Dim monthsToPay = Convert.ToInt32(numberOfMonths.Value)
                Dim loanAmountToPay = Convert.ToDecimal(loanAmount.Text.ToString)
                Dim monthlyPayment As Decimal
                Dim totalInterest As Decimal
                Dim totalCostOfLoan As Decimal
                monthlyPayment = (loanAmountToPay / monthsToPay) + ((loanAmountToPay / monthsToPay) * (currentRate / 100))
                totalInterest = ((loanAmountToPay / monthsToPay) * (currentRate / 100)) * monthsToPay
                totalCostOfLoan = loanAmountToPay + totalInterest
                con.Open()
                cmd = New MySqlCommand("INSERT INTO loans (`applicantID`, `loanAmount`,`dateApplied`,`isApproved`, `approvedBy`, `dueDate`, `runningBalance`, `monthlyPayment`, `numberOfPayments`, `totalInterest`,`totalCostOfLoan`, `base_rate`, `runningNumberOfMonths`)
                                Values(@applicantID, @loanAmount, @dateApplied, 0, NULL, @dueDate, @runningBalance, @monthlyPayment, @numberOfPayments, @totalInterest, @totalCostOfLoan, @base_rate,@runningNumberOfMonths) ", con)
                With cmd
                    .Parameters.AddWithValue("@applicantID", userID)
                    .Parameters.AddWithValue("@loanAmount", loanAmountToPay)
                    .Parameters.AddWithValue("@dateApplied", DateTime.Now)
                    .Parameters.AddWithValue("@dueDate", DateTime.Now.AddMonths(monthsToPay))
                    .Parameters.AddWithValue("@runningBalance", totalCostOfLoan)
                    .Parameters.AddWithValue("@monthlyPayment", monthlyPayment)
                    .Parameters.AddWithValue("@numberOfPayments", monthsToPay)
                    .Parameters.AddWithValue("@totalInterest", totalInterest)
                    .Parameters.AddWithValue("@totalCostOfLoan", totalCostOfLoan)
                    .Parameters.AddWithValue("@base_rate", currentRate)
                    .Parameters.AddWithValue("@runningNumberOfMonths", monthsToPay)
                    .ExecuteNonQuery()
                End With
                con.Close()
                loanAmount.Text = ""
                txtBoxmonthlyPayment.Text = ""
                txtBoxNumberOfPayment.Text = ""
                txtBoxTotalInterest.Text = ""
                txtBoxTotalCostOfLoan.Text = ""
                MsgBox("Successfully Requested Loan")
                RefreshLoanList()
            End If
        Else
            MsgBox("No user found", vbExclamation)
            con.Close()
        End If
    End Sub

    Private Function RefreshLoanList()
        MyLoanList.Rows.Clear()
        con.Open()
        cmd = New MySqlCommand("SELECT * FROM loans WHERE applicantID=@userID ORDER BY loanID DESC LIMIT 100", con)
        With cmd
            .Parameters.AddWithValue("@userID", userID)
        End With
        dr = cmd.ExecuteReader
        While dr.Read
            Dim isApproved As String
            If dr.Item("isApproved").ToString = "1" Then
                isApproved = "Yes"
            Else
                If dr.Item("isRejected").ToString = "1" Then
                    isApproved = "Rejected"
                Else
                    isApproved = "No"
                End If
            End If
            MyLoanList.Rows.Add(dr.Item("loanID").ToString,
                                $"PHP{dr.Item("loanAmount").ToString}",
                                isApproved,
                                dr.Item("dateApplied").ToString,
                                $"PHP{dr.Item("totalCostOfLoan").ToString}",
                                "Click to View"
                                )
            MyLoanList.ClearSelection()
        End While
        dr.Close()
        con.Close()
    End Function

    Private Sub clientFrmLoanApplication_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RefreshLoanList()
    End Sub

    Private Sub MyLoanList_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles MyLoanList.CellContentClick
        Dim columnName As String = MyLoanList.Columns(e.ColumnIndex).Name
        If columnName = "view" Then
            Dim selectedLoanID = MyLoanList.Rows(e.RowIndex).Cells(0).Value.ToString
            clientSelectedLoanID = selectedLoanID
            clientFrmLoanInfo.Show()
        End If
    End Sub

    Private Sub btnRefreshUserList_Click(sender As Object, e As EventArgs) Handles btnRefreshUserList.Click
        RefreshLoanList()
    End Sub
End Class