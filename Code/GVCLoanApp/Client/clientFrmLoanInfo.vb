﻿Imports MySql.Data.MySqlClient
Public Class clientFrmLoanInfo
    Private Sub btnDeposit_Click(sender As Object, e As EventArgs) Handles btnDeposit.Click
        Me.Close()
    End Sub

    Private Sub clientFrmLoanInfo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        con.Open()
        cmd = New MySqlCommand("SELECT * FROM loans WHERE loanID=@loanID LIMIT 1", con)
        With cmd
            .Parameters.AddWithValue("@loanID", clientSelectedLoanID)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                Dim isApproved As String
                txtBoxTotalInterest.Text = dr.Item("totalInterest").ToString
                txtBoxmonthlyPayment.Text = dr.Item("monthlyPayment").ToString
                txtBoxNumberOfPayment.Text = dr.Item("numberOfPayments").ToString
                txtBoxTotalCostOfLoan.Text = dr.Item("totalCostOfLoan").ToString
                txtBoxUserIDSelect.Text = dr.Item("applicantID").ToString
                loanAmount.Text = dr.Item("loanAmount").ToString
                numberOfMonths.Value = dr.Item("numberOfPayments").ToString
                interestRate.Value = dr.Item("base_rate").ToString
                remainingAmount.Text = dr.Item("runningBalance").ToString
                If Convert.ToInt32(dr.Item("isApproved")) = 1 Then
                    progressGroup.Visible = True
                End If
                If dr.Item("isApproved").ToString = "1" Then
                    isApproved = "Loan is Approved"
                    approveStatus.ForeColor = Color.Green
                Else
                    If dr.Item("isRejected").ToString = "1" Then
                        isApproved = "Loan is Rejected"
                        approveStatus.ForeColor = Color.Red
                        rejectNote.Text = $"Notes From Admin: {Environment.NewLine}{Environment.NewLine}{dr.Item("notes")}"
                        rejectNote.Visible = True
                        progressGroup.Visible = False
                    Else
                        isApproved = "Not yet Approved"
                        approveStatus.ForeColor = Color.DarkGray
                    End If
                End If
                approveStatus.Text = isApproved

            End While
        End If
        con.Close()

    End Sub
End Class