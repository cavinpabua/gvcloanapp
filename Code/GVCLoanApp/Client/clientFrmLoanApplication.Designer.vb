﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class clientFrmLoanApplication
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblUsersModule = New System.Windows.Forms.Label()
        Me.MetroTabControl1 = New MetroFramework.Controls.MetroTabControl()
        Me.MetroTabPage1 = New MetroFramework.Controls.MetroTabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtBoxmonthlyPayment = New Guna.UI2.WinForms.Guna2TextBox()
        Me.txtBoxNumberOfPayment = New Guna.UI2.WinForms.Guna2TextBox()
        Me.txtBoxTotalCostOfLoan = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtBoxTotalInterest = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Guna2Button2 = New Guna.UI2.WinForms.Guna2Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.numberOfMonths = New Guna.UI2.WinForms.Guna2NumericUpDown()
        Me.interestRate = New Guna.UI2.WinForms.Guna2NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Guna2Button1 = New Guna.UI2.WinForms.Guna2Button()
        Me.loanAmount = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.MetroTabPage2 = New MetroFramework.Controls.MetroTabPage()
        Me.btnRefreshUserList = New Guna.UI2.WinForms.Guna2Button()
        Me.MyLoanList = New Guna.UI2.WinForms.Guna2DataGridView()
        Me.loanID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.view = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        Me.MetroTabControl1.SuspendLayout()
        Me.MetroTabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.numberOfMonths, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.interestRate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage2.SuspendLayout()
        CType(Me.MyLoanList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblUsersModule)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(929, 49)
        Me.Panel1.TabIndex = 3
        '
        'lblUsersModule
        '
        Me.lblUsersModule.AutoSize = True
        Me.lblUsersModule.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsersModule.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblUsersModule.Location = New System.Drawing.Point(12, 9)
        Me.lblUsersModule.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblUsersModule.Name = "lblUsersModule"
        Me.lblUsersModule.Size = New System.Drawing.Size(160, 28)
        Me.lblUsersModule.TabIndex = 0
        Me.lblUsersModule.Text = "Loan Application"
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage1)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage2)
        Me.MetroTabControl1.Location = New System.Drawing.Point(17, 57)
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 1
        Me.MetroTabControl1.Size = New System.Drawing.Size(905, 484)
        Me.MetroTabControl1.TabIndex = 4
        Me.MetroTabControl1.UseSelectable = True
        '
        'MetroTabPage1
        '
        Me.MetroTabPage1.Controls.Add(Me.GroupBox1)
        Me.MetroTabPage1.Controls.Add(Me.Guna2Button2)
        Me.MetroTabPage1.Controls.Add(Me.Label6)
        Me.MetroTabPage1.Controls.Add(Me.Label5)
        Me.MetroTabPage1.Controls.Add(Me.numberOfMonths)
        Me.MetroTabPage1.Controls.Add(Me.interestRate)
        Me.MetroTabPage1.Controls.Add(Me.Label4)
        Me.MetroTabPage1.Controls.Add(Me.Guna2Button1)
        Me.MetroTabPage1.Controls.Add(Me.loanAmount)
        Me.MetroTabPage1.Controls.Add(Me.Label3)
        Me.MetroTabPage1.Controls.Add(Me.Label8)
        Me.MetroTabPage1.HorizontalScrollbarBarColor = True
        Me.MetroTabPage1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.HorizontalScrollbarSize = 2
        Me.MetroTabPage1.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage1.Name = "MetroTabPage1"
        Me.MetroTabPage1.Size = New System.Drawing.Size(897, 442)
        Me.MetroTabPage1.TabIndex = 0
        Me.MetroTabPage1.Text = "Open Loan"
        Me.MetroTabPage1.VerticalScrollbarBarColor = True
        Me.MetroTabPage1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.VerticalScrollbarSize = 4
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtBoxmonthlyPayment)
        Me.GroupBox1.Controls.Add(Me.txtBoxNumberOfPayment)
        Me.GroupBox1.Controls.Add(Me.txtBoxTotalCostOfLoan)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtBoxTotalInterest)
        Me.GroupBox1.Location = New System.Drawing.Point(411, 11)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(488, 364)
        Me.GroupBox1.TabIndex = 56
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Calculations"
        '
        'Label12
        '
        Me.Label12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(43, 277)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(162, 25)
        Me.Label12.TabIndex = 42
        Me.Label12.Text = "Total Cost of Loan"
        '
        'txtBoxmonthlyPayment
        '
        Me.txtBoxmonthlyPayment.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxmonthlyPayment.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxmonthlyPayment.DefaultText = ""
        Me.txtBoxmonthlyPayment.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxmonthlyPayment.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxmonthlyPayment.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxmonthlyPayment.DisabledState.Parent = Me.txtBoxmonthlyPayment
        Me.txtBoxmonthlyPayment.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxmonthlyPayment.Enabled = False
        Me.txtBoxmonthlyPayment.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxmonthlyPayment.FocusedState.Parent = Me.txtBoxmonthlyPayment
        Me.txtBoxmonthlyPayment.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxmonthlyPayment.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxmonthlyPayment.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxmonthlyPayment.HoverState.Parent = Me.txtBoxmonthlyPayment
        Me.txtBoxmonthlyPayment.Location = New System.Drawing.Point(48, 61)
        Me.txtBoxmonthlyPayment.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxmonthlyPayment.Name = "txtBoxmonthlyPayment"
        Me.txtBoxmonthlyPayment.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxmonthlyPayment.PlaceholderText = ""
        Me.txtBoxmonthlyPayment.ReadOnly = True
        Me.txtBoxmonthlyPayment.SelectedText = ""
        Me.txtBoxmonthlyPayment.ShadowDecoration.Parent = Me.txtBoxmonthlyPayment
        Me.txtBoxmonthlyPayment.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxmonthlyPayment.TabIndex = 35
        '
        'txtBoxNumberOfPayment
        '
        Me.txtBoxNumberOfPayment.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxNumberOfPayment.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxNumberOfPayment.DefaultText = ""
        Me.txtBoxNumberOfPayment.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxNumberOfPayment.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxNumberOfPayment.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxNumberOfPayment.DisabledState.Parent = Me.txtBoxNumberOfPayment
        Me.txtBoxNumberOfPayment.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxNumberOfPayment.Enabled = False
        Me.txtBoxNumberOfPayment.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxNumberOfPayment.FocusedState.Parent = Me.txtBoxNumberOfPayment
        Me.txtBoxNumberOfPayment.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxNumberOfPayment.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxNumberOfPayment.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxNumberOfPayment.HoverState.Parent = Me.txtBoxNumberOfPayment
        Me.txtBoxNumberOfPayment.Location = New System.Drawing.Point(48, 142)
        Me.txtBoxNumberOfPayment.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxNumberOfPayment.Name = "txtBoxNumberOfPayment"
        Me.txtBoxNumberOfPayment.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxNumberOfPayment.PlaceholderText = ""
        Me.txtBoxNumberOfPayment.ReadOnly = True
        Me.txtBoxNumberOfPayment.SelectedText = ""
        Me.txtBoxNumberOfPayment.ShadowDecoration.Parent = Me.txtBoxNumberOfPayment
        Me.txtBoxNumberOfPayment.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxNumberOfPayment.TabIndex = 37
        '
        'txtBoxTotalCostOfLoan
        '
        Me.txtBoxTotalCostOfLoan.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxTotalCostOfLoan.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxTotalCostOfLoan.DefaultText = ""
        Me.txtBoxTotalCostOfLoan.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.DisabledState.Parent = Me.txtBoxTotalCostOfLoan
        Me.txtBoxTotalCostOfLoan.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.Enabled = False
        Me.txtBoxTotalCostOfLoan.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.FocusedState.Parent = Me.txtBoxTotalCostOfLoan
        Me.txtBoxTotalCostOfLoan.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxTotalCostOfLoan.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.HoverState.Parent = Me.txtBoxTotalCostOfLoan
        Me.txtBoxTotalCostOfLoan.Location = New System.Drawing.Point(48, 311)
        Me.txtBoxTotalCostOfLoan.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxTotalCostOfLoan.Name = "txtBoxTotalCostOfLoan"
        Me.txtBoxTotalCostOfLoan.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxTotalCostOfLoan.PlaceholderText = ""
        Me.txtBoxTotalCostOfLoan.ReadOnly = True
        Me.txtBoxTotalCostOfLoan.SelectedText = ""
        Me.txtBoxTotalCostOfLoan.ShadowDecoration.Parent = Me.txtBoxTotalCostOfLoan
        Me.txtBoxTotalCostOfLoan.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxTotalCostOfLoan.TabIndex = 41
        '
        'Label10
        '
        Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(43, 111)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(188, 25)
        Me.Label10.TabIndex = 38
        Me.Label10.Text = "Number of Payments"
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(43, 30)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(159, 25)
        Me.Label7.TabIndex = 36
        Me.Label7.Text = "Monthly Payment"
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(43, 192)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(120, 25)
        Me.Label11.TabIndex = 40
        Me.Label11.Text = "Total Interest"
        '
        'txtBoxTotalInterest
        '
        Me.txtBoxTotalInterest.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxTotalInterest.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxTotalInterest.DefaultText = ""
        Me.txtBoxTotalInterest.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxTotalInterest.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxTotalInterest.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxTotalInterest.DisabledState.Parent = Me.txtBoxTotalInterest
        Me.txtBoxTotalInterest.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxTotalInterest.Enabled = False
        Me.txtBoxTotalInterest.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxTotalInterest.FocusedState.Parent = Me.txtBoxTotalInterest
        Me.txtBoxTotalInterest.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxTotalInterest.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxTotalInterest.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxTotalInterest.HoverState.Parent = Me.txtBoxTotalInterest
        Me.txtBoxTotalInterest.Location = New System.Drawing.Point(48, 223)
        Me.txtBoxTotalInterest.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxTotalInterest.Name = "txtBoxTotalInterest"
        Me.txtBoxTotalInterest.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxTotalInterest.PlaceholderText = ""
        Me.txtBoxTotalInterest.ReadOnly = True
        Me.txtBoxTotalInterest.SelectedText = ""
        Me.txtBoxTotalInterest.ShadowDecoration.Parent = Me.txtBoxTotalInterest
        Me.txtBoxTotalInterest.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxTotalInterest.TabIndex = 39
        '
        'Guna2Button2
        '
        Me.Guna2Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Guna2Button2.CheckedState.Parent = Me.Guna2Button2
        Me.Guna2Button2.CustomImages.Parent = Me.Guna2Button2
        Me.Guna2Button2.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Guna2Button2.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Guna2Button2.ForeColor = System.Drawing.Color.White
        Me.Guna2Button2.HoverState.Parent = Me.Guna2Button2
        Me.Guna2Button2.Location = New System.Drawing.Point(411, 387)
        Me.Guna2Button2.Name = "Guna2Button2"
        Me.Guna2Button2.ShadowDecoration.Parent = Me.Guna2Button2
        Me.Guna2Button2.Size = New System.Drawing.Size(488, 45)
        Me.Guna2Button2.TabIndex = 55
        Me.Guna2Button2.Text = "Request Loan"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(109, 233)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(76, 25)
        Me.Label6.TabIndex = 54
        Me.Label6.Text = "Months"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(-1, 199)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(165, 25)
        Me.Label5.TabIndex = 53
        Me.Label5.Text = "Period of Payment"
        '
        'numberOfMonths
        '
        Me.numberOfMonths.BackColor = System.Drawing.Color.Transparent
        Me.numberOfMonths.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.numberOfMonths.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.numberOfMonths.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.numberOfMonths.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.numberOfMonths.DisabledState.Parent = Me.numberOfMonths
        Me.numberOfMonths.DisabledState.UpDownButtonFillColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(177, Byte), Integer))
        Me.numberOfMonths.DisabledState.UpDownButtonForeColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(203, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.numberOfMonths.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.numberOfMonths.FocusedState.Parent = Me.numberOfMonths
        Me.numberOfMonths.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.numberOfMonths.ForeColor = System.Drawing.Color.FromArgb(CType(CType(126, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.numberOfMonths.Location = New System.Drawing.Point(3, 227)
        Me.numberOfMonths.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.numberOfMonths.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numberOfMonths.Name = "numberOfMonths"
        Me.numberOfMonths.ShadowDecoration.Parent = Me.numberOfMonths
        Me.numberOfMonths.Size = New System.Drawing.Size(100, 36)
        Me.numberOfMonths.TabIndex = 52
        Me.numberOfMonths.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'interestRate
        '
        Me.interestRate.BackColor = System.Drawing.Color.Transparent
        Me.interestRate.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.interestRate.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.interestRate.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.interestRate.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.interestRate.DisabledState.Parent = Me.interestRate
        Me.interestRate.DisabledState.UpDownButtonFillColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(177, Byte), Integer))
        Me.interestRate.DisabledState.UpDownButtonForeColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(203, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.interestRate.Enabled = False
        Me.interestRate.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.interestRate.FocusedState.Parent = Me.interestRate
        Me.interestRate.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.interestRate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(126, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.interestRate.Location = New System.Drawing.Point(3, 142)
        Me.interestRate.Name = "interestRate"
        Me.interestRate.ShadowDecoration.Parent = Me.interestRate
        Me.interestRate.Size = New System.Drawing.Size(100, 36)
        Me.interestRate.TabIndex = 51
        Me.interestRate.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(109, 144)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 32)
        Me.Label4.TabIndex = 50
        Me.Label4.Text = "%"
        '
        'Guna2Button1
        '
        Me.Guna2Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Guna2Button1.CheckedState.Parent = Me.Guna2Button1
        Me.Guna2Button1.CustomImages.Parent = Me.Guna2Button1
        Me.Guna2Button1.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Guna2Button1.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Guna2Button1.ForeColor = System.Drawing.Color.White
        Me.Guna2Button1.HoverState.Parent = Me.Guna2Button1
        Me.Guna2Button1.Location = New System.Drawing.Point(4, 288)
        Me.Guna2Button1.Name = "Guna2Button1"
        Me.Guna2Button1.ShadowDecoration.Parent = Me.Guna2Button1
        Me.Guna2Button1.Size = New System.Drawing.Size(383, 45)
        Me.Guna2Button1.TabIndex = 48
        Me.Guna2Button1.Text = "Calculate Payment"
        '
        'loanAmount
        '
        Me.loanAmount.BorderColor = System.Drawing.Color.DarkGray
        Me.loanAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.loanAmount.DefaultText = "0"
        Me.loanAmount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.loanAmount.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.loanAmount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.loanAmount.DisabledState.Parent = Me.loanAmount
        Me.loanAmount.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.loanAmount.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.loanAmount.FocusedState.Parent = Me.loanAmount
        Me.loanAmount.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.loanAmount.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.loanAmount.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.loanAmount.HoverState.Parent = Me.loanAmount
        Me.loanAmount.Location = New System.Drawing.Point(1, 47)
        Me.loanAmount.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.loanAmount.Name = "loanAmount"
        Me.loanAmount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.loanAmount.PlaceholderText = "Loan Amount"
        Me.loanAmount.SelectedText = ""
        Me.loanAmount.SelectionStart = 1
        Me.loanAmount.ShadowDecoration.Parent = Me.loanAmount
        Me.loanAmount.Size = New System.Drawing.Size(383, 44)
        Me.loanAmount.TabIndex = 45
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(-2, 107)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(192, 25)
        Me.Label3.TabIndex = 49
        Me.Label3.Text = "Monthly Interest Rate"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.White
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(-4, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(125, 25)
        Me.Label8.TabIndex = 47
        Me.Label8.Text = "Loan Amount"
        '
        'MetroTabPage2
        '
        Me.MetroTabPage2.Controls.Add(Me.btnRefreshUserList)
        Me.MetroTabPage2.Controls.Add(Me.MyLoanList)
        Me.MetroTabPage2.HorizontalScrollbarBarColor = True
        Me.MetroTabPage2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.HorizontalScrollbarSize = 2
        Me.MetroTabPage2.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage2.Name = "MetroTabPage2"
        Me.MetroTabPage2.Size = New System.Drawing.Size(897, 442)
        Me.MetroTabPage2.TabIndex = 1
        Me.MetroTabPage2.Text = "My Loan List"
        Me.MetroTabPage2.VerticalScrollbarBarColor = True
        Me.MetroTabPage2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.VerticalScrollbarSize = 4
        '
        'btnRefreshUserList
        '
        Me.btnRefreshUserList.CheckedState.Parent = Me.btnRefreshUserList
        Me.btnRefreshUserList.CustomImages.Parent = Me.btnRefreshUserList
        Me.btnRefreshUserList.FillColor = System.Drawing.Color.Transparent
        Me.btnRefreshUserList.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRefreshUserList.ForeColor = System.Drawing.Color.Black
        Me.btnRefreshUserList.HoverState.Parent = Me.btnRefreshUserList
        Me.btnRefreshUserList.Location = New System.Drawing.Point(3, 16)
        Me.btnRefreshUserList.Name = "btnRefreshUserList"
        Me.btnRefreshUserList.ShadowDecoration.Parent = Me.btnRefreshUserList
        Me.btnRefreshUserList.Size = New System.Drawing.Size(98, 23)
        Me.btnRefreshUserList.TabIndex = 4
        Me.btnRefreshUserList.Text = "Refresh List"
        '
        'MyLoanList
        '
        Me.MyLoanList.AllowUserToAddRows = False
        Me.MyLoanList.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        Me.MyLoanList.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MyLoanList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.MyLoanList.BackgroundColor = System.Drawing.Color.White
        Me.MyLoanList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MyLoanList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.MyLoanList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MyLoanList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MyLoanList.ColumnHeadersHeight = 52
        Me.MyLoanList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.loanID, Me.Column1, Me.Column2, Me.Column3, Me.Column5, Me.view})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MyLoanList.DefaultCellStyle = DataGridViewCellStyle4
        Me.MyLoanList.EnableHeadersVisualStyles = False
        Me.MyLoanList.GridColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MyLoanList.Location = New System.Drawing.Point(-1, 45)
        Me.MyLoanList.Name = "MyLoanList"
        Me.MyLoanList.ReadOnly = True
        Me.MyLoanList.RowHeadersVisible = False
        Me.MyLoanList.RowHeadersWidth = 51
        Me.MyLoanList.RowTemplate.Height = 24
        Me.MyLoanList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MyLoanList.Size = New System.Drawing.Size(898, 356)
        Me.MyLoanList.TabIndex = 2
        Me.MyLoanList.Theme = Guna.UI2.WinForms.Enums.DataGridViewPresetThemes.[Default]
        Me.MyLoanList.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White
        Me.MyLoanList.ThemeStyle.AlternatingRowsStyle.Font = Nothing
        Me.MyLoanList.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty
        Me.MyLoanList.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty
        Me.MyLoanList.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty
        Me.MyLoanList.ThemeStyle.BackColor = System.Drawing.Color.White
        Me.MyLoanList.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MyLoanList.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.MyLoanList.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.MyLoanList.ThemeStyle.HeaderStyle.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MyLoanList.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White
        Me.MyLoanList.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing
        Me.MyLoanList.ThemeStyle.HeaderStyle.Height = 52
        Me.MyLoanList.ThemeStyle.ReadOnly = True
        Me.MyLoanList.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White
        Me.MyLoanList.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.MyLoanList.ThemeStyle.RowsStyle.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MyLoanList.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.MyLoanList.ThemeStyle.RowsStyle.Height = 24
        Me.MyLoanList.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MyLoanList.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(71, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer))
        '
        'loanID
        '
        Me.loanID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.loanID.HeaderText = "Loan ID"
        Me.loanID.MinimumWidth = 2
        Me.loanID.Name = "loanID"
        Me.loanID.ReadOnly = True
        Me.loanID.Width = 95
        '
        'Column1
        '
        Me.Column1.HeaderText = "Loan Amount"
        Me.Column1.MinimumWidth = 6
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.HeaderText = "Is Approved"
        Me.Column2.MinimumWidth = 6
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.HeaderText = "Application Date"
        Me.Column3.MinimumWidth = 6
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "Total Cost Amount"
        Me.Column5.MinimumWidth = 6
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'view
        '
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ActiveCaption
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.view.DefaultCellStyle = DataGridViewCellStyle3
        Me.view.HeaderText = "Actions"
        Me.view.MinimumWidth = 6
        Me.view.Name = "view"
        Me.view.ReadOnly = True
        '
        'clientFrmLoanApplication
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(929, 584)
        Me.ControlBox = False
        Me.Controls.Add(Me.MetroTabControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "clientFrmLoanApplication"
        Me.Text = "clientFrmLoanApplication"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.MetroTabControl1.ResumeLayout(False)
        Me.MetroTabPage1.ResumeLayout(False)
        Me.MetroTabPage1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.numberOfMonths, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.interestRate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage2.ResumeLayout(False)
        CType(Me.MyLoanList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblUsersModule As Label
    Friend WithEvents MetroTabControl1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents MetroTabPage1 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage2 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtBoxmonthlyPayment As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents txtBoxNumberOfPayment As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents txtBoxTotalCostOfLoan As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txtBoxTotalInterest As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Guna2Button2 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents numberOfMonths As Guna.UI2.WinForms.Guna2NumericUpDown
    Friend WithEvents interestRate As Guna.UI2.WinForms.Guna2NumericUpDown
    Friend WithEvents Label4 As Label
    Friend WithEvents Guna2Button1 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents loanAmount As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents MyLoanList As Guna.UI2.WinForms.Guna2DataGridView
    Friend WithEvents btnRefreshUserList As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents loanID As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents view As DataGridViewTextBoxColumn
End Class
