﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class clientFrmLoanInfo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtBoxmonthlyPayment = New Guna.UI2.WinForms.Guna2TextBox()
        Me.txtBoxNumberOfPayment = New Guna.UI2.WinForms.Guna2TextBox()
        Me.txtBoxTotalCostOfLoan = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtBoxTotalInterest = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.numberOfMonths = New Guna.UI2.WinForms.Guna2NumericUpDown()
        Me.interestRate = New Guna.UI2.WinForms.Guna2NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.loanAmount = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtBoxUserIDSelect = New Guna.UI2.WinForms.Guna2TextBox()
        Me.progressGroup = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.remainingAmount = New Guna.UI2.WinForms.Guna2TextBox()
        Me.btnDeposit = New Guna.UI2.WinForms.Guna2Button()
        Me.approveStatus = New System.Windows.Forms.Label()
        Me.rejectNote = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.numberOfMonths, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.interestRate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.progressGroup.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(947, 52)
        Me.Panel1.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtBoxmonthlyPayment)
        Me.GroupBox1.Controls.Add(Me.txtBoxNumberOfPayment)
        Me.GroupBox1.Controls.Add(Me.txtBoxTotalCostOfLoan)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtBoxTotalInterest)
        Me.GroupBox1.Location = New System.Drawing.Point(434, 65)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(488, 364)
        Me.GroupBox1.TabIndex = 44
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Calculations"
        '
        'Label12
        '
        Me.Label12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(43, 277)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(162, 25)
        Me.Label12.TabIndex = 42
        Me.Label12.Text = "Total Cost of Loan"
        '
        'txtBoxmonthlyPayment
        '
        Me.txtBoxmonthlyPayment.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxmonthlyPayment.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxmonthlyPayment.DefaultText = ""
        Me.txtBoxmonthlyPayment.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxmonthlyPayment.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxmonthlyPayment.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxmonthlyPayment.DisabledState.Parent = Me.txtBoxmonthlyPayment
        Me.txtBoxmonthlyPayment.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxmonthlyPayment.Enabled = False
        Me.txtBoxmonthlyPayment.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxmonthlyPayment.FocusedState.Parent = Me.txtBoxmonthlyPayment
        Me.txtBoxmonthlyPayment.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxmonthlyPayment.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxmonthlyPayment.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxmonthlyPayment.HoverState.Parent = Me.txtBoxmonthlyPayment
        Me.txtBoxmonthlyPayment.Location = New System.Drawing.Point(48, 61)
        Me.txtBoxmonthlyPayment.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxmonthlyPayment.Name = "txtBoxmonthlyPayment"
        Me.txtBoxmonthlyPayment.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxmonthlyPayment.PlaceholderText = ""
        Me.txtBoxmonthlyPayment.ReadOnly = True
        Me.txtBoxmonthlyPayment.SelectedText = ""
        Me.txtBoxmonthlyPayment.ShadowDecoration.Parent = Me.txtBoxmonthlyPayment
        Me.txtBoxmonthlyPayment.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxmonthlyPayment.TabIndex = 35
        '
        'txtBoxNumberOfPayment
        '
        Me.txtBoxNumberOfPayment.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxNumberOfPayment.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxNumberOfPayment.DefaultText = ""
        Me.txtBoxNumberOfPayment.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxNumberOfPayment.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxNumberOfPayment.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxNumberOfPayment.DisabledState.Parent = Me.txtBoxNumberOfPayment
        Me.txtBoxNumberOfPayment.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxNumberOfPayment.Enabled = False
        Me.txtBoxNumberOfPayment.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxNumberOfPayment.FocusedState.Parent = Me.txtBoxNumberOfPayment
        Me.txtBoxNumberOfPayment.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxNumberOfPayment.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxNumberOfPayment.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxNumberOfPayment.HoverState.Parent = Me.txtBoxNumberOfPayment
        Me.txtBoxNumberOfPayment.Location = New System.Drawing.Point(48, 142)
        Me.txtBoxNumberOfPayment.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxNumberOfPayment.Name = "txtBoxNumberOfPayment"
        Me.txtBoxNumberOfPayment.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxNumberOfPayment.PlaceholderText = ""
        Me.txtBoxNumberOfPayment.ReadOnly = True
        Me.txtBoxNumberOfPayment.SelectedText = ""
        Me.txtBoxNumberOfPayment.ShadowDecoration.Parent = Me.txtBoxNumberOfPayment
        Me.txtBoxNumberOfPayment.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxNumberOfPayment.TabIndex = 37
        '
        'txtBoxTotalCostOfLoan
        '
        Me.txtBoxTotalCostOfLoan.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxTotalCostOfLoan.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxTotalCostOfLoan.DefaultText = ""
        Me.txtBoxTotalCostOfLoan.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.DisabledState.Parent = Me.txtBoxTotalCostOfLoan
        Me.txtBoxTotalCostOfLoan.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.Enabled = False
        Me.txtBoxTotalCostOfLoan.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.FocusedState.Parent = Me.txtBoxTotalCostOfLoan
        Me.txtBoxTotalCostOfLoan.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxTotalCostOfLoan.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxTotalCostOfLoan.HoverState.Parent = Me.txtBoxTotalCostOfLoan
        Me.txtBoxTotalCostOfLoan.Location = New System.Drawing.Point(48, 311)
        Me.txtBoxTotalCostOfLoan.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxTotalCostOfLoan.Name = "txtBoxTotalCostOfLoan"
        Me.txtBoxTotalCostOfLoan.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxTotalCostOfLoan.PlaceholderText = ""
        Me.txtBoxTotalCostOfLoan.ReadOnly = True
        Me.txtBoxTotalCostOfLoan.SelectedText = ""
        Me.txtBoxTotalCostOfLoan.ShadowDecoration.Parent = Me.txtBoxTotalCostOfLoan
        Me.txtBoxTotalCostOfLoan.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxTotalCostOfLoan.TabIndex = 41
        '
        'Label10
        '
        Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(43, 111)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(188, 25)
        Me.Label10.TabIndex = 38
        Me.Label10.Text = "Number of Payments"
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(43, 30)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(159, 25)
        Me.Label7.TabIndex = 36
        Me.Label7.Text = "Monthly Payment"
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(43, 192)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(120, 25)
        Me.Label11.TabIndex = 40
        Me.Label11.Text = "Total Interest"
        '
        'txtBoxTotalInterest
        '
        Me.txtBoxTotalInterest.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxTotalInterest.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxTotalInterest.DefaultText = ""
        Me.txtBoxTotalInterest.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxTotalInterest.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxTotalInterest.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxTotalInterest.DisabledState.Parent = Me.txtBoxTotalInterest
        Me.txtBoxTotalInterest.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxTotalInterest.Enabled = False
        Me.txtBoxTotalInterest.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxTotalInterest.FocusedState.Parent = Me.txtBoxTotalInterest
        Me.txtBoxTotalInterest.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxTotalInterest.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxTotalInterest.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxTotalInterest.HoverState.Parent = Me.txtBoxTotalInterest
        Me.txtBoxTotalInterest.Location = New System.Drawing.Point(48, 223)
        Me.txtBoxTotalInterest.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxTotalInterest.Name = "txtBoxTotalInterest"
        Me.txtBoxTotalInterest.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxTotalInterest.PlaceholderText = ""
        Me.txtBoxTotalInterest.ReadOnly = True
        Me.txtBoxTotalInterest.SelectedText = ""
        Me.txtBoxTotalInterest.ShadowDecoration.Parent = Me.txtBoxTotalInterest
        Me.txtBoxTotalInterest.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxTotalInterest.TabIndex = 39
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(125, 376)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(76, 25)
        Me.Label6.TabIndex = 54
        Me.Label6.Text = "Months"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(15, 342)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(165, 25)
        Me.Label5.TabIndex = 53
        Me.Label5.Text = "Period of Payment"
        '
        'numberOfMonths
        '
        Me.numberOfMonths.BackColor = System.Drawing.Color.Transparent
        Me.numberOfMonths.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.numberOfMonths.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.numberOfMonths.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.numberOfMonths.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.numberOfMonths.DisabledState.Parent = Me.numberOfMonths
        Me.numberOfMonths.DisabledState.UpDownButtonFillColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(177, Byte), Integer))
        Me.numberOfMonths.DisabledState.UpDownButtonForeColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(203, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.numberOfMonths.Enabled = False
        Me.numberOfMonths.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.numberOfMonths.FocusedState.Parent = Me.numberOfMonths
        Me.numberOfMonths.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.numberOfMonths.ForeColor = System.Drawing.Color.FromArgb(CType(CType(126, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.numberOfMonths.Location = New System.Drawing.Point(19, 370)
        Me.numberOfMonths.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.numberOfMonths.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numberOfMonths.Name = "numberOfMonths"
        Me.numberOfMonths.ShadowDecoration.Parent = Me.numberOfMonths
        Me.numberOfMonths.Size = New System.Drawing.Size(100, 36)
        Me.numberOfMonths.TabIndex = 52
        Me.numberOfMonths.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'interestRate
        '
        Me.interestRate.BackColor = System.Drawing.Color.Transparent
        Me.interestRate.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.interestRate.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.interestRate.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.interestRate.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.interestRate.DisabledState.Parent = Me.interestRate
        Me.interestRate.DisabledState.UpDownButtonFillColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(177, Byte), Integer))
        Me.interestRate.DisabledState.UpDownButtonForeColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(203, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.interestRate.Enabled = False
        Me.interestRate.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.interestRate.FocusedState.Parent = Me.interestRate
        Me.interestRate.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.interestRate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(126, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.interestRate.Location = New System.Drawing.Point(19, 285)
        Me.interestRate.Name = "interestRate"
        Me.interestRate.ShadowDecoration.Parent = Me.interestRate
        Me.interestRate.Size = New System.Drawing.Size(100, 36)
        Me.interestRate.TabIndex = 51
        Me.interestRate.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(125, 287)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 32)
        Me.Label4.TabIndex = 50
        Me.Label4.Text = "%"
        '
        'loanAmount
        '
        Me.loanAmount.BorderColor = System.Drawing.Color.DarkGray
        Me.loanAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.loanAmount.DefaultText = "0"
        Me.loanAmount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.loanAmount.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.loanAmount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.loanAmount.DisabledState.Parent = Me.loanAmount
        Me.loanAmount.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.loanAmount.Enabled = False
        Me.loanAmount.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.loanAmount.FocusedState.Parent = Me.loanAmount
        Me.loanAmount.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.loanAmount.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.loanAmount.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.loanAmount.HoverState.Parent = Me.loanAmount
        Me.loanAmount.Location = New System.Drawing.Point(17, 190)
        Me.loanAmount.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.loanAmount.Name = "loanAmount"
        Me.loanAmount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.loanAmount.PlaceholderText = "Loan Amount"
        Me.loanAmount.ReadOnly = True
        Me.loanAmount.SelectedText = ""
        Me.loanAmount.SelectionStart = 1
        Me.loanAmount.ShadowDecoration.Parent = Me.loanAmount
        Me.loanAmount.Size = New System.Drawing.Size(383, 44)
        Me.loanAmount.TabIndex = 46
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(14, 250)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(192, 25)
        Me.Label3.TabIndex = 49
        Me.Label3.Text = "Monthly Interest Rate"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(12, 159)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(125, 25)
        Me.Label8.TabIndex = 48
        Me.Label8.Text = "Loan Amount"
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(12, 65)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(73, 25)
        Me.Label9.TabIndex = 47
        Me.Label9.Text = "User ID"
        '
        'txtBoxUserIDSelect
        '
        Me.txtBoxUserIDSelect.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxUserIDSelect.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxUserIDSelect.DefaultText = ""
        Me.txtBoxUserIDSelect.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxUserIDSelect.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxUserIDSelect.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxUserIDSelect.DisabledState.Parent = Me.txtBoxUserIDSelect
        Me.txtBoxUserIDSelect.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxUserIDSelect.Enabled = False
        Me.txtBoxUserIDSelect.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxUserIDSelect.FocusedState.Parent = Me.txtBoxUserIDSelect
        Me.txtBoxUserIDSelect.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxUserIDSelect.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxUserIDSelect.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxUserIDSelect.HoverState.Parent = Me.txtBoxUserIDSelect
        Me.txtBoxUserIDSelect.Location = New System.Drawing.Point(17, 96)
        Me.txtBoxUserIDSelect.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBoxUserIDSelect.Name = "txtBoxUserIDSelect"
        Me.txtBoxUserIDSelect.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxUserIDSelect.PlaceholderText = "User ID"
        Me.txtBoxUserIDSelect.SelectedText = ""
        Me.txtBoxUserIDSelect.ShadowDecoration.Parent = Me.txtBoxUserIDSelect
        Me.txtBoxUserIDSelect.Size = New System.Drawing.Size(383, 44)
        Me.txtBoxUserIDSelect.TabIndex = 45
        '
        'progressGroup
        '
        Me.progressGroup.Controls.Add(Me.Label1)
        Me.progressGroup.Controls.Add(Me.remainingAmount)
        Me.progressGroup.Location = New System.Drawing.Point(20, 444)
        Me.progressGroup.Name = "progressGroup"
        Me.progressGroup.Size = New System.Drawing.Size(407, 126)
        Me.progressGroup.TabIndex = 55
        Me.progressGroup.TabStop = False
        Me.progressGroup.Text = "Progress"
        Me.progressGroup.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(6, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(173, 25)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Remaining Amount"
        '
        'remainingAmount
        '
        Me.remainingAmount.BorderColor = System.Drawing.Color.DarkGray
        Me.remainingAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.remainingAmount.DefaultText = ""
        Me.remainingAmount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.remainingAmount.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.remainingAmount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.remainingAmount.DisabledState.Parent = Me.remainingAmount
        Me.remainingAmount.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.remainingAmount.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.remainingAmount.FocusedState.Parent = Me.remainingAmount
        Me.remainingAmount.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.remainingAmount.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.remainingAmount.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.remainingAmount.HoverState.Parent = Me.remainingAmount
        Me.remainingAmount.Location = New System.Drawing.Point(8, 64)
        Me.remainingAmount.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.remainingAmount.Name = "remainingAmount"
        Me.remainingAmount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.remainingAmount.PlaceholderText = "Remaining Amount"
        Me.remainingAmount.ReadOnly = True
        Me.remainingAmount.SelectedText = ""
        Me.remainingAmount.ShadowDecoration.Parent = Me.remainingAmount
        Me.remainingAmount.Size = New System.Drawing.Size(361, 44)
        Me.remainingAmount.TabIndex = 43
        '
        'btnDeposit
        '
        Me.btnDeposit.BackColor = System.Drawing.SystemColors.Control
        Me.btnDeposit.CheckedState.Parent = Me.btnDeposit
        Me.btnDeposit.CustomImages.Parent = Me.btnDeposit
        Me.btnDeposit.FillColor = System.Drawing.Color.IndianRed
        Me.btnDeposit.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeposit.ForeColor = System.Drawing.Color.White
        Me.btnDeposit.HoverState.Parent = Me.btnDeposit
        Me.btnDeposit.Location = New System.Drawing.Point(790, 486)
        Me.btnDeposit.Name = "btnDeposit"
        Me.btnDeposit.ShadowDecoration.Parent = Me.btnDeposit
        Me.btnDeposit.Size = New System.Drawing.Size(132, 45)
        Me.btnDeposit.TabIndex = 44
        Me.btnDeposit.Text = "Close"
        '
        'approveStatus
        '
        Me.approveStatus.AutoSize = True
        Me.approveStatus.Font = New System.Drawing.Font("Segoe UI", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.approveStatus.ForeColor = System.Drawing.Color.Red
        Me.approveStatus.Location = New System.Drawing.Point(488, 486)
        Me.approveStatus.Name = "approveStatus"
        Me.approveStatus.Size = New System.Drawing.Size(220, 32)
        Me.approveStatus.TabIndex = 56
        Me.approveStatus.Text = "Not Yet Approved"
        '
        'rejectNote
        '
        Me.rejectNote.BackColor = System.Drawing.Color.White
        Me.rejectNote.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rejectNote.Location = New System.Drawing.Point(19, 412)
        Me.rejectNote.Multiline = True
        Me.rejectNote.Name = "rejectNote"
        Me.rejectNote.Size = New System.Drawing.Size(383, 160)
        Me.rejectNote.TabIndex = 72
        Me.rejectNote.Text = "Reject Note"
        Me.rejectNote.Visible = False
        '
        'clientFrmLoanInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(947, 582)
        Me.Controls.Add(Me.rejectNote)
        Me.Controls.Add(Me.approveStatus)
        Me.Controls.Add(Me.btnDeposit)
        Me.Controls.Add(Me.progressGroup)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.numberOfMonths)
        Me.Controls.Add(Me.interestRate)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.loanAmount)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtBoxUserIDSelect)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "clientFrmLoanInfo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "clientFrmLoanInfo"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.numberOfMonths, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.interestRate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.progressGroup.ResumeLayout(False)
        Me.progressGroup.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtBoxmonthlyPayment As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents txtBoxNumberOfPayment As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents txtBoxTotalCostOfLoan As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txtBoxTotalInterest As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents numberOfMonths As Guna.UI2.WinForms.Guna2NumericUpDown
    Friend WithEvents interestRate As Guna.UI2.WinForms.Guna2NumericUpDown
    Friend WithEvents Label4 As Label
    Friend WithEvents loanAmount As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txtBoxUserIDSelect As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents progressGroup As GroupBox
    Friend WithEvents remainingAmount As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents btnDeposit As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Label1 As Label
    Friend WithEvents approveStatus As Label
    Friend WithEvents rejectNote As TextBox
End Class
