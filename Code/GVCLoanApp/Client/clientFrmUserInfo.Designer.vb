﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class clientFrmUserInfo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblUsersModule = New System.Windows.Forms.Label()
        Me.txtBoxSourceOfIncome = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.comboBoxMaritalStatus = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dateTimePickerDOB = New Guna.UI2.WinForms.Guna2DateTimePicker()
        Me.txtBoxFirstname = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBoxLastName = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBoxUsername = New Guna.UI2.WinForms.Guna2TextBox()
        Me.btnRegister = New Guna.UI2.WinForms.Guna2Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.savingsBalanceText = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblUsersModule)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(929, 46)
        Me.Panel1.TabIndex = 2
        '
        'lblUsersModule
        '
        Me.lblUsersModule.AutoSize = True
        Me.lblUsersModule.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsersModule.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblUsersModule.Location = New System.Drawing.Point(42, 9)
        Me.lblUsersModule.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblUsersModule.Name = "lblUsersModule"
        Me.lblUsersModule.Size = New System.Drawing.Size(90, 28)
        Me.lblUsersModule.TabIndex = 0
        Me.lblUsersModule.Text = "User Info"
        '
        'txtBoxSourceOfIncome
        '
        Me.txtBoxSourceOfIncome.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBoxSourceOfIncome.AutoSize = True
        Me.txtBoxSourceOfIncome.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxSourceOfIncome.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxSourceOfIncome.DefaultText = ""
        Me.txtBoxSourceOfIncome.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxSourceOfIncome.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxSourceOfIncome.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxSourceOfIncome.DisabledState.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxSourceOfIncome.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxSourceOfIncome.FocusedState.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxSourceOfIncome.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxSourceOfIncome.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxSourceOfIncome.HoverState.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.Location = New System.Drawing.Point(475, 158)
        Me.txtBoxSourceOfIncome.Margin = New System.Windows.Forms.Padding(6, 9, 6, 9)
        Me.txtBoxSourceOfIncome.Name = "txtBoxSourceOfIncome"
        Me.txtBoxSourceOfIncome.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxSourceOfIncome.PlaceholderText = "Source Of Income"
        Me.txtBoxSourceOfIncome.SelectedText = ""
        Me.txtBoxSourceOfIncome.ShadowDecoration.Parent = Me.txtBoxSourceOfIncome
        Me.txtBoxSourceOfIncome.Size = New System.Drawing.Size(342, 44)
        Me.txtBoxSourceOfIncome.TabIndex = 23
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(470, 124)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(162, 25)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Source Of Income"
        '
        'comboBoxMaritalStatus
        '
        Me.comboBoxMaritalStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboBoxMaritalStatus.Animated = True
        Me.comboBoxMaritalStatus.BackColor = System.Drawing.Color.Transparent
        Me.comboBoxMaritalStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.comboBoxMaritalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboBoxMaritalStatus.FocusedColor = System.Drawing.Color.Empty
        Me.comboBoxMaritalStatus.FocusedState.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.comboBoxMaritalStatus.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.comboBoxMaritalStatus.FormattingEnabled = True
        Me.comboBoxMaritalStatus.HoverState.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.ItemHeight = 30
        Me.comboBoxMaritalStatus.Items.AddRange(New Object() {"Single", "Married", "Separated", "Divorced", "Widowed"})
        Me.comboBoxMaritalStatus.ItemsAppearance.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.Location = New System.Drawing.Point(475, 65)
        Me.comboBoxMaritalStatus.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.comboBoxMaritalStatus.MinimumSize = New System.Drawing.Size(186, 0)
        Me.comboBoxMaritalStatus.Name = "comboBoxMaritalStatus"
        Me.comboBoxMaritalStatus.ShadowDecoration.Parent = Me.comboBoxMaritalStatus
        Me.comboBoxMaritalStatus.Size = New System.Drawing.Size(342, 36)
        Me.comboBoxMaritalStatus.TabIndex = 21
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(481, 21)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(127, 25)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Marital Status"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(470, 221)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(118, 25)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Date of Birth"
        '
        'dateTimePickerDOB
        '
        Me.dateTimePickerDOB.CheckedState.Parent = Me.dateTimePickerDOB
        Me.dateTimePickerDOB.FillColor = System.Drawing.Color.Transparent
        Me.dateTimePickerDOB.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateTimePickerDOB.HoverState.Parent = Me.dateTimePickerDOB
        Me.dateTimePickerDOB.Location = New System.Drawing.Point(475, 255)
        Me.dateTimePickerDOB.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.dateTimePickerDOB.MaxDate = New Date(9998, 12, 31, 0, 0, 0, 0)
        Me.dateTimePickerDOB.MinDate = New Date(1753, 1, 1, 0, 0, 0, 0)
        Me.dateTimePickerDOB.Name = "dateTimePickerDOB"
        Me.dateTimePickerDOB.ShadowDecoration.Parent = Me.dateTimePickerDOB
        Me.dateTimePickerDOB.Size = New System.Drawing.Size(216, 46)
        Me.dateTimePickerDOB.TabIndex = 26
        Me.dateTimePickerDOB.Value = New Date(2021, 9, 29, 23, 49, 4, 893)
        '
        'txtBoxFirstname
        '
        Me.txtBoxFirstname.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxFirstname.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxFirstname.DefaultText = ""
        Me.txtBoxFirstname.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxFirstname.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxFirstname.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxFirstname.DisabledState.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxFirstname.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxFirstname.FocusedState.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxFirstname.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxFirstname.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxFirstname.HoverState.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.Location = New System.Drawing.Point(34, 158)
        Me.txtBoxFirstname.Margin = New System.Windows.Forms.Padding(6, 9, 6, 9)
        Me.txtBoxFirstname.Name = "txtBoxFirstname"
        Me.txtBoxFirstname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxFirstname.PlaceholderText = "First Name"
        Me.txtBoxFirstname.SelectedText = ""
        Me.txtBoxFirstname.ShadowDecoration.Parent = Me.txtBoxFirstname
        Me.txtBoxFirstname.Size = New System.Drawing.Size(367, 44)
        Me.txtBoxFirstname.TabIndex = 19
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(31, 221)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 25)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "Last Name"
        '
        'txtBoxLastName
        '
        Me.txtBoxLastName.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxLastName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxLastName.DefaultText = ""
        Me.txtBoxLastName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxLastName.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxLastName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxLastName.DisabledState.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxLastName.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxLastName.FocusedState.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxLastName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxLastName.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxLastName.HoverState.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.Location = New System.Drawing.Point(34, 255)
        Me.txtBoxLastName.Margin = New System.Windows.Forms.Padding(6, 9, 6, 9)
        Me.txtBoxLastName.Name = "txtBoxLastName"
        Me.txtBoxLastName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxLastName.PlaceholderText = "Last Name"
        Me.txtBoxLastName.SelectedText = ""
        Me.txtBoxLastName.ShadowDecoration.Parent = Me.txtBoxLastName
        Me.txtBoxLastName.Size = New System.Drawing.Size(367, 46)
        Me.txtBoxLastName.TabIndex = 20
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(29, 124)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(102, 25)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "First Name"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(29, 21)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 25)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Username"
        '
        'txtBoxUsername
        '
        Me.txtBoxUsername.BorderColor = System.Drawing.Color.DarkGray
        Me.txtBoxUsername.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBoxUsername.DefaultText = ""
        Me.txtBoxUsername.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.txtBoxUsername.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.txtBoxUsername.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxUsername.DisabledState.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtBoxUsername.Enabled = False
        Me.txtBoxUsername.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxUsername.FocusedState.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoxUsername.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtBoxUsername.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBoxUsername.HoverState.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.Location = New System.Drawing.Point(34, 58)
        Me.txtBoxUsername.Margin = New System.Windows.Forms.Padding(6, 9, 6, 9)
        Me.txtBoxUsername.Name = "txtBoxUsername"
        Me.txtBoxUsername.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBoxUsername.PlaceholderText = "Username"
        Me.txtBoxUsername.ReadOnly = True
        Me.txtBoxUsername.SelectedText = ""
        Me.txtBoxUsername.ShadowDecoration.Parent = Me.txtBoxUsername
        Me.txtBoxUsername.Size = New System.Drawing.Size(367, 45)
        Me.txtBoxUsername.TabIndex = 18
        '
        'btnRegister
        '
        Me.btnRegister.BackColor = System.Drawing.SystemColors.Control
        Me.btnRegister.CheckedState.Parent = Me.btnRegister
        Me.btnRegister.CustomImages.Parent = Me.btnRegister
        Me.btnRegister.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.btnRegister.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegister.ForeColor = System.Drawing.Color.White
        Me.btnRegister.HoverState.Parent = Me.btnRegister
        Me.btnRegister.Location = New System.Drawing.Point(34, 348)
        Me.btnRegister.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnRegister.Name = "btnRegister"
        Me.btnRegister.ShadowDecoration.Parent = Me.btnRegister
        Me.btnRegister.Size = New System.Drawing.Size(367, 44)
        Me.btnRegister.TabIndex = 31
        Me.btnRegister.Text = "Save Update"
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.Controls.Add(Me.btnRegister)
        Me.Panel2.Controls.Add(Me.dateTimePickerDOB)
        Me.Panel2.Controls.Add(Me.txtBoxSourceOfIncome)
        Me.Panel2.Controls.Add(Me.txtBoxUsername)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.comboBoxMaritalStatus)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.txtBoxLastName)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.txtBoxFirstname)
        Me.Panel2.Location = New System.Drawing.Point(15, 129)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(901, 420)
        Me.Panel2.TabIndex = 32
        '
        'savingsBalanceText
        '
        Me.savingsBalanceText.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.savingsBalanceText.AutoSize = True
        Me.savingsBalanceText.Location = New System.Drawing.Point(47, 38)
        Me.savingsBalanceText.Name = "savingsBalanceText"
        Me.savingsBalanceText.Size = New System.Drawing.Size(22, 25)
        Me.savingsBalanceText.TabIndex = 36
        Me.savingsBalanceText.Text = "0"
        Me.savingsBalanceText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(42, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(225, 25)
        Me.Label7.TabIndex = 35
        Me.Label7.Text = "Savings Account Balance:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.savingsBalanceText)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(0, 46)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(929, 75)
        Me.Panel3.TabIndex = 33
        '
        'Label8
        '
        Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label8.Location = New System.Drawing.Point(273, 10)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(122, 25)
        Me.Label8.TabIndex = 37
        Me.Label8.Text = "Show History"
        '
        'clientFrmUserInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(929, 647)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "clientFrmUserInfo"
        Me.Text = "clientFrmUserInfo"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblUsersModule As Label
    Friend WithEvents txtBoxSourceOfIncome As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents comboBoxMaritalStatus As Guna.UI2.WinForms.Guna2ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents dateTimePickerDOB As Guna.UI2.WinForms.Guna2DateTimePicker
    Friend WithEvents txtBoxFirstname As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtBoxLastName As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtBoxUsername As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents btnRegister As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents savingsBalanceText As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label8 As Label
End Class
