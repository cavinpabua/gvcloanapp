﻿Imports MySql.Data.MySqlClient
Public Class clientFrmUserInfo
    Private Function loadSavingsBalance() As Decimal
        con.Open()
        cmd = New MySqlCommand("SELECT runningBalance FROM savings_account WHERE userID=@userID LIMIT 1", con)
        With cmd
            .Parameters.AddWithValue("@userID", userID)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                savingsBalanceText.Text = $"PHP{dr.Item("runningBalance").ToString}"
            End While
        End If
        dr.Close()
        con.Close()

    End Function

    Private Function loadUserInfo()
        con.Open()
        cmd = New MySqlCommand("SELECT * FROM users WHERE userID=@userID LIMIT 1", con)
        With cmd
            .Parameters.AddWithValue("@userID", userID)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                txtBoxUsername.Text = dr.Item("username").ToString
                txtBoxFirstname.Text = dr.Item("firstName").ToString
                txtBoxLastName.Text = dr.Item("lastName").ToString
                txtBoxSourceOfIncome.Text = dr.Item("sourceOfIncome").ToString
                comboBoxMaritalStatus.SelectedItem = dr.Item("maritalStatus").ToString
                dateTimePickerDOB.Value = dr.Item("dob")
            End While
        End If
        dr.Close()
        con.Close()

    End Function

    Private Function saveInfoUpdate()
        Try
            con.Open()
            cmd = New MySqlCommand("UPDATE users set firstName=@firstName, lastName=@lastName,sourceOfIncome=@sourceOfIncome, maritalStatus=@maritalStatus, dob=@dob WHERE userID=@userID", con)
            With cmd
                .Parameters.AddWithValue("@userID", userID)
                .Parameters.AddWithValue("@firstName", txtBoxFirstname.Text)
                .Parameters.AddWithValue("@lastName", txtBoxLastName.Text)
                .Parameters.AddWithValue("@sourceOfIncome", txtBoxSourceOfIncome.Text)
                .Parameters.AddWithValue("@maritalStatus", comboBoxMaritalStatus.SelectedItem)
                .Parameters.AddWithValue("@dob", dateTimePickerDOB.Value.Date)
                .ExecuteNonQuery()
            End With
            con.Close()
            clientFrmMain.lblUsername.Text = txtBoxFirstname.Text + " " + txtBoxLastName.Text
            MsgBox("Successfully Saved")
        Catch ex As Exception
            MsgBox("An error Occured", vbExclamation)
        End Try
    End Function

    Private Sub clientFrmUserInfo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadSavingsBalance()
        loadUserInfo()
    End Sub

    Private Sub Label8_Click(sender As Object, e As EventArgs) Handles Label8.Click
        clientFrmSavingsHistory.Show()
    End Sub

    Private Sub Panel2_Paint(sender As Object, e As PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click
        saveInfoUpdate()
    End Sub
End Class