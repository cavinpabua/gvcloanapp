﻿Public Class clientFrmMain
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        picUser.Left = (Panel3.Width - picUser.Width) / 2
        openCon()
        clientSystemLabel.Text = initSystemName()
        lblUsername.Text = userFirstName + " " + userLastName
        lblUserLevel.Text = userRole

        With clientFrmUserInfo
            .Width = clientMainPanel.Width
            .Height = clientMainPanel.Height
            .TopLevel = False
            clientMainPanel.Controls.Add(clientFrmUserInfo)
            .BringToFront()
            .Show()
        End With

    End Sub
    Sub CloseForms()
        For i As Integer = My.Application.OpenForms.Count - 1 To 0 Step -1
            If My.Application.OpenForms(i) IsNot Me Then My.Application.OpenForms(i).Dispose()
        Next
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs)
        Me.Hide()
        userFrmLogin.Show()
    End Sub

    Private Sub btnExit_Click_1(sender As Object, e As EventArgs) Handles btnExit.Click
        CloseForms()
        Me.Hide()
        userFrmLogin.Show()
    End Sub

    Private Sub btnLoanApplications_Click(sender As Object, e As EventArgs) Handles btnLoanApplications.Click
        For Each f As Form In My.Application.OpenForms
            If f.Name = frmUsers.Name Then Return
        Next
        CloseForms()
        With clientFrmLoanApplication
            .Width = clientMainPanel.Width
            .Height = clientMainPanel.Height
            .TopLevel = False
            clientMainPanel.Controls.Add(clientFrmLoanApplication)
            .BringToFront()
            .Show()
        End With
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        For Each f As Form In My.Application.OpenForms
            If f.Name = frmUsers.Name Then Return
        Next
        CloseForms()
        With clientFrmUserInfo
            .Width = clientMainPanel.Width
            .Height = clientMainPanel.Height
            .TopLevel = False
            clientMainPanel.Controls.Add(clientFrmUserInfo)
            .BringToFront()
            .Show()
        End With
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        For Each f As Form In My.Application.OpenForms
            If f.Name = frmUsers.Name Then Return
        Next
        CloseForms()
        With clientFrmSettings
            .Width = clientMainPanel.Width
            .Height = clientMainPanel.Height
            .TopLevel = False
            clientMainPanel.Controls.Add(clientFrmSettings)
            .BringToFront()
            .Show()
        End With
    End Sub
End Class