﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class clientFrmSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblUsersModule = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.NewPassConfirm = New Guna.UI2.WinForms.Guna2TextBox()
        Me.NewPass = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Guna2Button2 = New Guna.UI2.WinForms.Guna2Button()
        Me.oldPass = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblUsersModule)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(929, 49)
        Me.Panel1.TabIndex = 4
        '
        'lblUsersModule
        '
        Me.lblUsersModule.AutoSize = True
        Me.lblUsersModule.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsersModule.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblUsersModule.Location = New System.Drawing.Point(12, 9)
        Me.lblUsersModule.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblUsersModule.Name = "lblUsersModule"
        Me.lblUsersModule.Size = New System.Drawing.Size(83, 28)
        Me.lblUsersModule.TabIndex = 0
        Me.lblUsersModule.Text = "Settings"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Panel2.Controls.Add(Me.NewPassConfirm)
        Me.Panel2.Controls.Add(Me.NewPass)
        Me.Panel2.Controls.Add(Me.Guna2Button2)
        Me.Panel2.Controls.Add(Me.oldPass)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Location = New System.Drawing.Point(17, 57)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(512, 415)
        Me.Panel2.TabIndex = 21
        '
        'NewPassConfirm
        '
        Me.NewPassConfirm.BorderColor = System.Drawing.Color.DarkGray
        Me.NewPassConfirm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.NewPassConfirm.DefaultText = ""
        Me.NewPassConfirm.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.NewPassConfirm.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.NewPassConfirm.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.NewPassConfirm.DisabledState.Parent = Me.NewPassConfirm
        Me.NewPassConfirm.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.NewPassConfirm.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.NewPassConfirm.FocusedState.Parent = Me.NewPassConfirm
        Me.NewPassConfirm.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NewPassConfirm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.NewPassConfirm.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.NewPassConfirm.HoverState.Parent = Me.NewPassConfirm
        Me.NewPassConfirm.Location = New System.Drawing.Point(17, 172)
        Me.NewPassConfirm.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.NewPassConfirm.Name = "NewPassConfirm"
        Me.NewPassConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NewPassConfirm.PlaceholderText = "Confirm New Password"
        Me.NewPassConfirm.SelectedText = ""
        Me.NewPassConfirm.ShadowDecoration.Parent = Me.NewPassConfirm
        Me.NewPassConfirm.Size = New System.Drawing.Size(258, 44)
        Me.NewPassConfirm.TabIndex = 25
        Me.NewPassConfirm.UseSystemPasswordChar = True
        '
        'NewPass
        '
        Me.NewPass.BorderColor = System.Drawing.Color.DarkGray
        Me.NewPass.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.NewPass.DefaultText = ""
        Me.NewPass.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.NewPass.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.NewPass.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.NewPass.DisabledState.Parent = Me.NewPass
        Me.NewPass.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.NewPass.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.NewPass.FocusedState.Parent = Me.NewPass
        Me.NewPass.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NewPass.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.NewPass.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.NewPass.HoverState.Parent = Me.NewPass
        Me.NewPass.Location = New System.Drawing.Point(17, 110)
        Me.NewPass.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.NewPass.Name = "NewPass"
        Me.NewPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NewPass.PlaceholderText = "New Password"
        Me.NewPass.SelectedText = ""
        Me.NewPass.ShadowDecoration.Parent = Me.NewPass
        Me.NewPass.Size = New System.Drawing.Size(258, 44)
        Me.NewPass.TabIndex = 24
        Me.NewPass.UseSystemPasswordChar = True
        '
        'Guna2Button2
        '
        Me.Guna2Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Guna2Button2.CheckedState.Parent = Me.Guna2Button2
        Me.Guna2Button2.CustomImages.Parent = Me.Guna2Button2
        Me.Guna2Button2.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(211, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.Guna2Button2.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Guna2Button2.ForeColor = System.Drawing.Color.White
        Me.Guna2Button2.HoverState.Parent = Me.Guna2Button2
        Me.Guna2Button2.Location = New System.Drawing.Point(17, 235)
        Me.Guna2Button2.Name = "Guna2Button2"
        Me.Guna2Button2.ShadowDecoration.Parent = Me.Guna2Button2
        Me.Guna2Button2.Size = New System.Drawing.Size(146, 45)
        Me.Guna2Button2.TabIndex = 23
        Me.Guna2Button2.Text = "Save Password"
        '
        'oldPass
        '
        Me.oldPass.BorderColor = System.Drawing.Color.DarkGray
        Me.oldPass.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.oldPass.DefaultText = ""
        Me.oldPass.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.oldPass.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.oldPass.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.oldPass.DisabledState.Parent = Me.oldPass
        Me.oldPass.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.oldPass.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.oldPass.FocusedState.Parent = Me.oldPass
        Me.oldPass.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.oldPass.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.oldPass.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.oldPass.HoverState.Parent = Me.oldPass
        Me.oldPass.Location = New System.Drawing.Point(17, 54)
        Me.oldPass.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.oldPass.Name = "oldPass"
        Me.oldPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.oldPass.PlaceholderText = "Old Password"
        Me.oldPass.SelectedText = ""
        Me.oldPass.ShadowDecoration.Parent = Me.oldPass
        Me.oldPass.Size = New System.Drawing.Size(258, 44)
        Me.oldPass.TabIndex = 21
        Me.oldPass.UseSystemPasswordChar = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(12, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(169, 25)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "Change Password"
        '
        'clientFrmSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(929, 584)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Segoe UI", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "clientFrmSettings"
        Me.Text = "clientFrmSettings"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblUsersModule As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents NewPassConfirm As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents NewPass As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Guna2Button2 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents oldPass As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label3 As Label
End Class
