﻿Imports MySql.Data.MySqlClient
Public Class clientFrmSavingsHistory
    Private Sub clientFrmSavingsHistory_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        con.Open()
        cmd = New MySqlCommand("SELECT transactionID, amount, type, createdAt FROM savings_transaction WHERE userID=@userID ORDER BY transactionID DESC LIMIT 100", con)
        With cmd
            .Parameters.AddWithValue("@userID", userID)
        End With
        dr = cmd.ExecuteReader
        If dr.HasRows Then
            While dr.Read
                depositHistory.Rows.Add(dr.Item("transactionID").ToString, dr.Item("amount").ToString, dr.Item("type").ToString, dr.Item("createdAt").ToString)
                depositHistory.ClearSelection()
            End While
        End If
        con.Close()
    End Sub

    Private Sub btnDeposit_Click(sender As Object, e As EventArgs) Handles btnDeposit.Click
        Me.Close()
    End Sub
End Class