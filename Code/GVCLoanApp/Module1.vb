﻿Imports MySql.Data.MySqlClient

Module Module1
    Public con As New MySqlConnection
    Public cmd As New MySqlCommand
    Public dr As MySqlDataReader
    Public userRole As String
    Public userFirstName As String
    Public userLastName As String
    Public userDOB As String
    Public userMaritalStatus As String
    Public userSourceOfIncome As String
    Public userID As String

    Public userIDWithdrawalSelected As String
    Public savingsIDWithdrawalSelected As String
    Public userIDWithdrawalSelectedRunningBalance = 0

    Public userIDDepositSelected As String
    Public savingsIDDepositSelected As String

    Public clientSelectedLoanID As String
    Public frmSelectedToViewLoanID As String
    Public frmSelectedToPayLoanID As String

    Public frmSelectedToAcceptLoanID As String



    Public Function is_Empty(ByVal obj As Object) As Boolean
        If obj.text = String.Empty Then
            MsgBox("Please fill up " + obj.PlaceholderText, vbInformation)
            obj.focus
            is_Empty = True
        Else
            is_Empty = False
        End If
    End Function
    Sub openCon()
        Try
            con.ConnectionString = "Server=localhost;uid=root;password=;database=gvc_db;SslMode=none;convert zero datetime=True"
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical)
        End Try
    End Sub

    Public Function initSystemName() As String
        Dim TheTitle As String
        con.Open()
        cmd = New MySqlCommand("SELECT * FROM settings WHERE id=1", con)
        dr = cmd.ExecuteReader
        While dr.Read
            TheTitle = dr.Item("app_name")
        End While
        dr.Close()
        con.Close()
        Return TheTitle
    End Function

    Public Function getInterestRate() As Integer
        Dim TheRate As Integer
        con.Open()
        cmd = New MySqlCommand("SELECT * FROM settings WHERE id=1", con)
        dr = cmd.ExecuteReader
        While dr.Read
            TheRate = dr.Item("base_rate")
        End While
        dr.Close()
        con.Close()
        Return TheRate
    End Function
End Module
